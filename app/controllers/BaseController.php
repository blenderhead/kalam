<?php

	class BaseController extends Controller 
	{
		public $rate;

		public function __construct()
		{
			$credential = Sentry::getUser();

			if($credential)
			{
				$the_user = User::find($credential->id);
				View::share('the_user', $the_user);
			}
			
			$total_books = Book::all()->count();
			$total_authors = Author::all()->count();
			$total_publishers = Publisher::all()->count();

			$container_class = 'bg-grey-100';
			$invoice_head = '';

			if (Route::currentRouteName() == 'sale.edit' || Route::currentRouteName() == 'report.per_publisher') 
			{
				$container_class = 'bg-white';
				$invoice_head = 'margin-bottom-20';
			}

			$default_currency = Session::get('convert_from') ? Session::get('convert_from') : 'IDR';

			View::share('credential', $credential);
			View::share('current_route_name', Route::currentRouteName());
			View::share('total_books', $total_books);
			View::share('total_authors', $total_authors);
			View::share('total_publishers', $total_publishers);
			View::share('container_class', $container_class);
			View::share('invoice_head', $invoice_head);
			View::share('default_currency', $default_currency);
			
		}
		
		protected function setupLayout()
		{
			if ( ! is_null($this->layout))
			{
				$this->layout = View::make($this->layout);
			}
		}

	}
