<?php

	class BookController extends BaseController 
	{
		public function getIndex()
		{
            $data['books'] = Book::all();
			return View::make('book.index', $data);
		}

		public function getCreate()
		{
            $data['categories'] = Category::all();
            $data['publishers'] = Publisher::all();
            $data['authors'] = Author::all();
			return View::make('book.add', $data);
		}

		public function postCreate()
		{
			$form_processor = new BookAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new BookAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function getEdit()
		{
            $id = Input::get('id');
            $data['book'] = Book::find($id);
            $data['categories'] = Category::all();
            $data['publishers'] = Publisher::all();
            $data['authors'] = Author::all();
			return View::make('book.edit', $data);
		}

		public function postEdit()
		{
			$form_processor = new BookEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new BookEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function postDelete()
		{
			$form_processor = new BookDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new BookDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

        public function getAddEdition()
        {
            $id = Input::get('id');
            $data['book'] = Book::find($id);
            $data['categories'] = Category::all();
            $data['publishers'] = Publisher::all();
            $data['authors'] = Author::all();
            $data['last_edition'] = Book::getLastEdition($data['book']->book_code);
            return View::make('book.add_edition', $data);
        }

        public function postAddEdition()
        {
            $form_processor = new BookAddEditionRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new BookAddEditionProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function getEditEdition()
        {
            $id = Input::get('id');
            $data['book'] = Book::find($id);
            $data['categories'] = Category::all();
            $data['publishers'] = Publisher::all();
            $data['authors'] = Author::all();
            $data['last_edition'] = Book::getLastEdition($data['book']->book_code);
            return View::make('book.edit_edition', $data);
        }

        public function postEditEdition()
        {
            $form_processor = new BookEditEditionRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new BookEditEditionProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }
	}
