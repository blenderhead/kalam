<?php

	class CurrencyController extends BaseController 
	{
		public function setCurrency()
		{
			$from = Input::get('from');
			$to = Input::get('to');

			Session::put('convert_from', $from);
			Session::put('convert_to', $to);
		}
	}
