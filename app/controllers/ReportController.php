<?php

    class ReportController extends BaseController 
    {
        public function getMaster()
        {
            $data['publishers'] = Publisher::all();
            return View::make('report.master', $data);
        }

        public function postMaster()
        {
            $form_processor = new GetMasterRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new GetMasterProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('output', $processor->getOutput()), 200);
            }
        }

        public function getPerPublisher()
        {
            $data['publishers'] = Publisher::all();
            $data['year'] = date('Y', time());
            $data['total_royalties'] = Publisher::getAllPublisherRoyalties(date('Y', time()));
            return View::make('report.publisher', $data);
        }

        public function postPerPublisher()
        {
            $form_processor = new GetPubRoyalRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new GetPubRoyalProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('output', $processor->getOutput()), 200);
            }
        }

        public function generateMasterPdf()
        {
            $data = $this->getBooks();

            $input['books'] = $data['books'];
            $input['year'] = $data['year'];
            $input['publisher_filter'] = $data['publisher'];
            $input['publisher'] = $input['publisher_filter'];
            $view = View::make('report.pdf.master_pdf',$input)->render();
            $pdf = App::make('dompdf');
            $pdf->loadHTML($view)->setPaper('a4')->setOrientation('landscape');
            return $pdf->stream();
        }

        public function generateMasterPrint()
        {
            $data = $this->getBooks();

            $input['books'] = $data['books'];
            $input['year'] = $data['year'];
            $input['publisher_filter'] = $data['publisher'];
            $input['publisher'] = $input['publisher_filter'];
            return View::make('report.print.master_print',$data);
        }

        public function generatePublisherPdf()
        {
            $data['year'] = Input::get('year');
            $data['publishers'] = Publisher::all();
            $data['total_royalties'] = Publisher::getAllPublisherRoyalties($data['year']);
            $view = View::make('report.pdf.publisher_pdf',$data)->render();
            $pdf = App::make('dompdf');
            $pdf->loadHTML($view)->setPaper('a4');
            return $pdf->stream();
        }

        public function generatePublisherPrint()
        {
            $data['year'] = Input::get('year');
            $data['publishers'] = Publisher::all();
            $data['total_royalties'] = Publisher::getAllPublisherRoyalties($data['year']);
            return View::make('report.print.publisher_print',$data);
        }

        private function getBooks()
        {
            $year = Input::get('year');
            $publisher = Input::get('publisher');

            if($publisher)
            {
                $books = Book::where('publisher_id','=',$publisher)->get();
                $publisher_data = Publisher::find($publisher);
                $publisher = $publisher_data;
            }
            else
            {
                $books = Book::all();
                $publisher = 0;
            }
            
            $result = array();

            $books->each(function($book) use (&$result, $year) {

                $total_royalty = Helper::calculateRoyalty($book->id, $year, $book->royalty->royalty_from_price, $book->adv_royalty);
                
                $royalty_paid = Helper::calculatePaidRoyalty($total_royalty, $book->adv_royalty, $book);

                $adv_royalty = Helper::calculateAdvancedRoyalty($book);

                $book->printed = Stock::getTotalPrints($book->id, $year);
                $book->adv_royalty = Helper::convertCurrency($adv_royalty);
                $book->stock = Stock::getTotalPrints($book->id, $year) - Sale::getTotalYearSales($book->id, $year);
                $book->sales = Sale::getTotalYearSales($book->id, $year);
                $book->sales_nominal = Helper::convertCurrency(Helper::getSalesNominal($book->id, $year, $book->price));
                $book->total_royalty = Helper::convertCurrency($total_royalty);
                $book->royalty_paid = Helper::convertCurrency($royalty_paid);
                $book->first_print_date = Helper::processPrintYear($book, 'first_print');
                $book->last_print_date = Helper::processPrintYear($book, 'last_print');
            });

            return compact('year', 'publisher', 'books');
        }
    }