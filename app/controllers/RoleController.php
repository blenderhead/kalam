<?php

	class RoleController extends BaseController 
	{
		public function getIndex()
		{
            $data['roles'] = Role::getRoles();
			return View::make('user.role.index', $data);
		}

        public function getCreate()
        {
            return View::make('user.role.add');
        }

        public function postCreate()
        {
            $form_processor = new RoleAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new RoleAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function getEdit()
        {
            $id = Input::get('id');
            $data['role'] = Role::find($id);
            return View::make('user.role.edit', $data);
        }

        public function postEdit()
        {
            $form_processor = new RoleEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new RoleEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function postDelete()
        {
            $form_processor = new RoleDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new RoleDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }
	}
