<?php

	class SaleController extends BaseController 
	{
		public function getIndex()
		{
			$data['books'] = Book::all();
			return View::make('sale.index', $data);
		}

		public function getEdit()
		{
            $id = Input::get('id');
            $data['book'] = Book::getBook($id);
            $book = new Book();
            $data['books'] = $book->getBooks();
            $data['total_sales'] = Sale::getTotalSales($id, date('Y', time()));
            $data['total_stock'] = Stock::getTotalStock($id, date('Y', time()));
            $data['book_sales'] = Sale::getBookSales($id, date('Y', time()));
            $data['year'] = date('Y', time());
			return View::make('sale.edit', $data);
		}

		public function postSave()
		{
			$form_processor = new SaleSaveRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new SaleSaveProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('output', $processor->getOutput()), 200);
            }
		}

        public function postDelete()
        {
            $form_processor = new SaleDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new SaleDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('output', $processor->getOutput()), 200);
            }
        }

        public function postGetSale()
        {
            $form_processor = new SaleGetRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new SaleGetProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('output', $processor->getOutput()), 200);
            }
        }

        public function generatePrint()
        {
            $book_id = Input::get('book_id');
            $year = Input::get('year');
            $data['book'] = Book::getBook($book_id);
            $book = new Book();
            $data['books'] = $book->getBooks();
            $data['total_sales'] = Sale::getTotalSales($book_id, $year);
            $data['total_stock'] = Stock::getTotalStock($book_id, $year);
            $data['book_sales'] = Sale::getBookSales($book_id, $year);
            $data['year'] = $year;
            return View::make('sale.print.print',$data);
        }

        public function generatePdf()
        {
            $book_id = Input::get('book_id');
            $year = Input::get('year');
            $data['book'] = Book::getBook($book_id);
            $book = new Book();
            $data['books'] = $book->getBooks();
            $data['total_sales'] = Sale::getTotalSales($book_id, $year);
            $data['total_stock'] = Stock::getTotalStock($book_id, $year);
            $data['book_sales'] = Sale::getBookSales($book_id, $year);
            $data['year'] = $year;
            $view = View::make('sale.pdf.pdf',$data)->render();
            //$pdf = PDF::loadView('sale.pdf.pdf', $data);
            //return $pdf->download($data['book']->title . "(Sale Summary - $year)" . '.pdf');
            $pdf = App::make('dompdf');
            $pdf->loadHTML($view);
            return $pdf->stream();
            //return View::make('sale.pdf.pdf',$data);
        }
	}
