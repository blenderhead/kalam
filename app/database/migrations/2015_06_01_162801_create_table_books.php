<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBooks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('books', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('author_id');
		    $table->integer('publisher_id');
		    $table->string('title');
		    $table->integer('category_id');
		    $table->date('published_date');
		    $table->string('edition');
		    $table->date('contact_date');
		    $table->string('contact_number');
		    $table->date('first_print_date');
		    $table->date('last_print_date');
		    $table->string('isbn');
		    $table->string('image');
	    	$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('books');
	}

}
