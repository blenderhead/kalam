<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRoyaltiesAddRoyaltyFromPrice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('royalties', function(Blueprint $table)
		{
		    $table->decimal('royalty_from_price')->after('amount');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('royalties', function(Blueprint $table)
		{
		    $table->dropColumn('royalty_from_price');
		});
	}

}
