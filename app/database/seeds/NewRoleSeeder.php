<?php

    class NewRoleSeeder extends Seeder
    {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $admin = new Role;
            $admin->name = 'Admin Royalty';
            $admin->save();

            $stock_dashboard = new Permission;
            $stock_dashboard->name = 'input_stock';
            $stock_dashboard->display_name = 'Input Stock Permission';
            $stock_dashboard->save();

            $royalty_dashboard = new Permission;
            $royalty_dashboard->name = 'input_royalty';
            $royalty_dashboard->display_name = 'Input Royalty Permission';
            $royalty_dashboard->save();

            $sale_dashboard = new Permission;
            $sale_dashboard->name = 'input_sales';
            $sale_dashboard->display_name = 'Manage Sales Permission';
            $sale_dashboard->save();

            $report_dashboard = new Permission;
            $report_dashboard->name = 'view_report';
            $report_dashboard->display_name = 'View report Permission';
            $report_dashboard->save();
        }

    }
