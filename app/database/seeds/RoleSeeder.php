<?php

    class RoleSeeder extends Seeder
    {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $admin = new Role;
            $admin->name = 'Admin';
            $admin->save();

            $keuangan = new Role;
            $keuangan->name = 'Finance';
            $keuangan->save();

            $stockist = new Role;
            $stockist->name = 'Stockist';
            $stockist->save(); 

            $gudang = new Role;
            $gudang->name = 'Warehouse';
            $gudang->save(); 

            $executive = new Role;
            $executive->name = 'Executive';
            $executive->save(); 

            $admin_dashboard = new Permission;
            $admin_dashboard->name = 'access_all';
            $admin_dashboard->display_name = 'All Access Permission';
            $admin_dashboard->save();

            $finance_dashboard = new Permission;
            $finance_dashboard->name = 'access_report';
            $finance_dashboard->display_name = 'Finance Dashboard Permission';
            $finance_dashboard->save();

            $stockist_dashboard = new Permission;
            $stockist_dashboard->name = 'manage_stock_access';
            $stockist_dashboard->display_name = 'Stockist Dashboard Permission';
            $stockist_dashboard->save();

            $entry_dashboard = new Permission;
            $entry_dashboard->name = 'input_data';
            $entry_dashboard->display_name = 'Manage Data Permission';
            $entry_dashboard->save();

            $executive_dashboard = new Permission;
            $executive_dashboard->name = 'access_executive_data';
            $executive_dashboard->display_name = 'Executive Dashboard Permission';
            $executive_dashboard->save();

            $admin->perms()->sync(array($admin_dashboard->id));
            $keuangan->perms()->sync(array($finance_dashboard->id));
            $stockist->perms()->sync(array($stockist_dashboard->id));
            $gudang->perms()->sync(array($entry_dashboard->id));
            $executive->perms()->sync(array($executive_dashboard->id));

            $user = \Sentry::getUserProvider()->create([
                'email' => 'admin@kalam.dev',
                'password' => '12345678',
                'activated' => '1',
                'first_name' => 'Admin',
                'last_name' => 'Kalam',
                'role' => strtolower(Role::getRoleName($admin->id))
            ]);
    
            $user_entity = User::where('id','=',$user->id)->first();
            $user_entity->roles()->attach($admin->id);
        }

    }
