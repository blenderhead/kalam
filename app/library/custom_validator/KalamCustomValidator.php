<?php
	
	class KalamCustomValidator extends \Illuminate\Validation\Validator
	{
		public function validateCheckStock($attribute, $value, $parameters)
		{
			$sale_amount = $parameters[0];
			$book_id = $parameters[1];
			
			$total_sales = Sale::getTotalSales($book_id, date('Y', time()));
            $total_stock = Stock::getTotalStock($book_id, date('Y', time()));

            $in_stock = $total_stock - $total_sales;

            if($sale_amount > $in_stock)
            {
            	return FALSE;
            }

            return TRUE;
		}

		public function validateDateGreaterThan($attribute, $value, $parameters)
		{
			$this->requireParameterCount(1, $parameters, 'greater than');
			$value = strtotime($value);
			$other = strtotime(Input::get($parameters[0]));

			if($value > $other || $value == $other)
			{
				return TRUE;
			}

			//return isset($other) and $value > $other;
		}

		public function validateDateLessThan($attribute, $value, $parameters)
		{
			$this->requireParameterCount(1, $parameters, 'less than');
			$value = strtotime($value);
			$other = strtotime(Input::get($parameters[0]));

			if($value < $other || $value == $other)
			{
				return TRUE;
			}

			//return isset($other) and $value < $other;
		}

		public function validateCheckContractRange($attribute, $value, $parameters)
		{
			$first_print_date = strtotime($value);
			$start_contract_date = strtotime($parameters[0]);
			$end_contract_date = strtotime($parameters[1]);

			if($first_print_date > $start_contract_date && $first_print_date < $end_contract_date)
			{
				return TRUE;
			}

			return FALSE;
		}

		protected function replaceDateLessThan($message, $attribute, $rule, $parameters)
		{
		    return str_replace(':other', 'end date', $message);
		}

		protected function replaceDateGreaterThan($message, $attribute, $rule, $parameters)
		{
		    return str_replace(':other', 'start date', $message);
		}
	}	
