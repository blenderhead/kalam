<?php
	
	class AuthorDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$author = Author::find($data['id']);

				//Author::where('id','=',$author->id)->delete();
				Author::where('id','=',$author->id)->update(array(
					'is_deleted' => 1
				));

				Helper::deleteImage(Config::get('path.author_upload_path') . $author->image);

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}