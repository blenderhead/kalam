<?php
	
	class AuthorEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$author = Author::find($data['author_id']);
				$author->name = $data['author_name'];
				$author->email = $data['author_email'];
				$author->gender = $data['author_gender'];
				$author->description = $data['author_description'];
				$author->image = Helper::updateImage($data['image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.author_upload_path'), $author->image);
				$author->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}