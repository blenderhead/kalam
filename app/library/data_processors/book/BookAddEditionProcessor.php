<?php
	
	class BookAddEditionProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$book_data = Book::find($data['book_id']);

				$book = new Book();
				$book->book_code = $book_data->book_code;
				$book->title = $book_data->title;
				$book->original_title = $book_data->original_title;
				$book->description = $data['description'];
				$book->author_id = $book_data->author_id;
				$book->publisher_id = $book_data->publisher_id;
				$book->category_id = $book_data->category_id;
				$book->published_date = $data['last_print_date'];
				$book->edition = $data['edition'];
				$book->contact_date = $data['contract_date'];
				$book->end_contract_date = $book_data['end_contract_date'];
				$book->contact_number = $data['contract_number'];
				$book->last_print_date = $data['last_print_date'];
				$book->isbn = $data['isbn_number'];
				$book->price = $data['price'];
				$book->image = Helper::processImage($data['image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.book_upload_path'));
				$book->contract_image = Helper::processImage($data['logo'], $data['logo_coord_x'], $data['logo_coord_y'], $data['logo_w'], $data['logo_h'], $data['logo_rw'], Config::get('path.book_upload_path'));
				$book->adv_royalty = $data['advanced_royalty'];
				$book->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}