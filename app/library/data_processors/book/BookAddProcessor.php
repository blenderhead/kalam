<?php
	
	class BookAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				if(Book::checkEdition($data['title'],$data['author'],$data['publisher'],$data['edition'],'add') == TRUE)
				{
					$book = new Book();
					$book->book_code = $data['code'];
					$book->title = $data['title'];
					$book->original_title = $data['original_title'];
					$book->description = $data['description'];
					$book->author_id = $data['author'];
					$book->publisher_id = $data['publisher'];
					$book->category_id = $data['category'];
					$book->edition = $data['edition'];
					$book->contact_date = $data['contract_date'];
					$book->end_contract_date = $data['end_contract_date'];
					$book->contact_number = $data['contract_number'];
					
					$book->isbn = $data['isbn_number'];
					$book->price = $data['price'];
					$book->image = Helper::processImage($data['image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.book_upload_path'));
					$book->contract_image = Helper::processImage($data['logo'], $data['logo_coord_x'], $data['logo_coord_y'], $data['logo_w'], $data['logo_h'], $data['logo_rw'], Config::get('path.book_upload_path'));
					$book->adv_royalty = $data['advanced_royalty'];

					$book->published_date = $data['first_print_date'];
					$book->last_print_date = '0000-00-00';
					$book->first_print_date = $data['first_print_date'];

					$book->save();

					$royalty = new Royalty();
					$royalty->book_id = $book->id;
					$royalty->amount = 0;
					$royalty->royalty_from_price = 0;
					$royalty->save();

					//$publisher = Publisher::find($data['publisher']);
					//$book = Book::find($book->id);
					//$book->book_code = $publisher->publisher_code . Helper::processCode($data['category']) . Helper::processCode($book->id);
					//$book->book_code = $data['book_code'];
					//$book->save();

					return TRUE;
				}
				else
				{
					throw new Exception("Book for the edition is already exist", 1000);
					return FALSE;
				}
				
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}