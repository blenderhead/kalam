<?php
	
	class BookDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$book = Book::find($data['id']);

				Book::where('id','=',$book->id)->delete();

				Helper::deleteImage(Config::get('path.book_upload_path') . $book->image);
				Helper::deleteImage(Config::get('path.book_upload_path') . $book->contract_image);
				
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}