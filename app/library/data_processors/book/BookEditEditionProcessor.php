<?php
	
	class BookEditEditionProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$book = Book::find($data['book_id']);

				if(Book::checkEdition($book->title,$book->author,$book->publisher,$book->edition,'edit',$book->id) == TRUE)
				{
					$book->description = $data['description'];
					$book->contact_date = $data['contract_date'];
					$book->end_contract_date = $data['end_contract_date'];
					$book->contact_number = $data['contract_number'];

					$book->published_date = $data['last_print_date'];
					$book->first_print_date = $data['last_print_date'];
					$book->last_print_date = date('Y-m-d H:i:s', strtotime($data['last_print_date']));
					
					$book->isbn = $data['isbn_number'];
					$book->price = $data['price'];
					$book->image = Helper::updateImage($data['image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.book_upload_path'), $book->image);
					$book->contract_image = Helper::updateImage($data['logo'], $data['logo_coord_x'], $data['logo_coord_y'], $data['logo_w'], $data['logo_h'], $data['logo_rw'], Config::get('path.book_upload_path'), $book->contract_image);
					$book->adv_royalty = $data['advanced_royalty'];
					$book->save();

					return TRUE;
				}
				else
				{
					throw new Exception("Book for the edition is already exist", 1000);
					return FALSE;
				}
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}