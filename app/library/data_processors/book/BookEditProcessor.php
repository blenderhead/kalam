<?php
	
	class BookEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				if(Book::checkEdition($data['title'],$data['author'],$data['publisher'],$data['edition'],'edit',$data['book_id']) == TRUE)
				{
					$book = Book::find($data['book_id']);
					$book->title = $data['title'];
					$book->original_title = $data['original_title'];
					$book->description = $data['description'];
					$book->author_id = $data['author'];
					$book->publisher_id = $data['publisher'];
					$book->category_id = $data['category'];
					$book->published_date = $data['first_print_date'];
					$book->edition = $data['edition'];
					$book->contact_date = $data['contract_date'];
					$book->end_contract_date = $data['end_contract_date'];
					$book->contact_number = $data['contract_number'];
					$book->first_print_date = $data['first_print_date'];
					$book->last_print_date = $data['last_print_date'];
					$book->isbn = $data['isbn_number'];
					$book->price = $data['price'];
					$book->image = Helper::updateImage($data['image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.book_upload_path'), $book->image);
					$book->contract_image = Helper::updateImage($data['logo'], $data['logo_coord_x'], $data['logo_coord_y'], $data['logo_w'], $data['logo_h'], $data['logo_rw'], Config::get('path.book_upload_path'), $book->contract_image);
					$book->adv_royalty = $data['advanced_royalty'];
					$book->save();

					BookEditProcessor::editEdition($book);

					return TRUE;
				}
				else
				{
					throw new Exception("Book for the edition is already exist", 1000);
					return FALSE;
				}
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}

		public static function editEdition($book)
		{
			$editions = Book::where('book_code','=',$book->book_code)->where('id','!=',$book->id)->get();

			if(!$editions->isEmpty())
			{
				$editions->each(function($edition) use ($book) {
					$edition->title = $book->title;
					$edition->original_title = $book->original_title;
					$edition->author_id = $book->author_id;
					$edition->publisher_id = $book->publisher_id;
					$edition->category_id = $book->category_id;
					$edition->save();
				});
			}
		}
	}