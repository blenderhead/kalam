<?php
	
	class CategoryAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$category = new Category();
				$category->name = ucwords($data['category_name']);
				$category->code = ucwords($data['category_code']);
				$category->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}