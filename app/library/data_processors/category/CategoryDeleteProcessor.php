<?php
	
	class CategoryDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				//Category::where('id','=',$data['id'])->delete();
				Category::where('id','=',$data['id'])->update(array(
					'is_deleted' => 1
				));

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}