<?php
	
	class CategoryEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$category = Category::find($data['category_id']);
				$category->name = ucwords($data['category_name']);
				$category->code = ucwords($data['category_code']);
				$category->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}