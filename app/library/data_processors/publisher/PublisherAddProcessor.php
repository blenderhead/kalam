<?php
	
	class PublisherAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$publisher = new Publisher();
				$publisher->name = $data['publisher_name'];
				$publisher->publisher_code = $data['publisher_code'];
				$publisher->description = $data['publisher_description'];
				$publisher->address = $data['address'];
				$publisher->bank = $data['bank'];
				$publisher->account_number = $data['account_number'];
				$publisher->image = Helper::processImage($data['image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.publisher_upload_path'));
				$publisher->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}