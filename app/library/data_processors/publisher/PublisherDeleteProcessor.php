<?php
	
	class PublisherDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$publisher = Publisher::find($data['id']);

				//Publisher::where('id','=',$publisher->id)->delete();

				Publisher::where('id','=',$publisher->id)->update(array(
					'is_deleted' => 1
				));

				Helper::deleteImage(Config::get('path.publisher_upload_path') . $publisher->image);

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}