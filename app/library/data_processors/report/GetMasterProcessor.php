<?php
	
	class GetMasterProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$year = $data['year'];
				$publisher = $data['publisher'];

				if($publisher)
				{
					$books = Book::where('publisher_id','=',$publisher)->get();
					$publisher_data = Publisher::find($publisher);
                	$input['publisher'] = $publisher_data;
				}
				else
				{
					$books = Book::all();
					$input['publisher'] = 0;
				}
				
				$result = array();

				$books->each(function($book) use (&$result, $year) {

					$total_royalty = Helper::calculateRoyalty($book->id, $year, $book->royalty->royalty_from_price, $book->adv_royalty);
					
					$royalty_paid = Helper::calculatePaidRoyalty($total_royalty, $book->adv_royalty, $book);

					$adv_royalty = Helper::calculateAdvancedRoyalty($book);

					$book->printed = Stock::getTotalPrints($book->id, $year);
					$book->adv_royalty = Helper::convertCurrency($adv_royalty);
					$book->stock = Stock::getTotalPrints($book->id, $year) - Sale::getTotalYearSales($book->id, $year);
					$book->sales = Sale::getTotalYearSales($book->id, $year);
					$book->sales_nominal = Helper::convertCurrency(Helper::getSalesNominal($book->id, $year, $book->price));
					$book->total_royalty = Helper::convertCurrency($total_royalty);
					$book->royalty_paid = Helper::convertCurrency($royalty_paid);
					$book->first_print_date = Helper::processPrintYear($book, 'first_print');
					$book->last_print_date = Helper::processPrintYear($book, 'last_print');
					$book->adv_royalty_balance = Helper::convertCurrency($book->adv_royalty_balance);
				});
				
				$input['books'] = $books;
				$input['publisher_filter'] = $publisher;
				$input['year'] = $year;
				$this->output = View::make('report.ajax.master_ajax', $input)->render();

	            return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}