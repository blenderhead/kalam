<?php
	
	class GetPubRoyalProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$input['publishers'] = Publisher::all();
	            $input['year'] = $data['year'];
	            $input['total_royalties'] = Publisher::getAllPublisherRoyalties($data['year']);
	            $this->output = View::make('report.ajax.publisher_ajax', $input)->render();
	            return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}