<?php
	
	class RoyaltyAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$royalty = Royalty::where('book_id','=',$data['book_id'])->first();

				if(!$royalty)
				{
					$royalty = new Royalty();
				}

				$royalty->book_id = $data['book_id'];
				$royalty->amount = $data['royalty'];

				$book = Book::find($data['book_id']);
				$royalty->royalty_from_price = ($book->price * $data['royalty'])/100;
				$royalty->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}