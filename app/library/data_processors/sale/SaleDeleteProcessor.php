<?php
	
	class SaleDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				Sale::where('id','=',$data['id'])->delete();
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}