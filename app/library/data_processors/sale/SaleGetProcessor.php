<?php
	
	class SaleGetProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$input['total_sales'] = Sale::getTotalSales($data['book_id'], $data['year']);
	            $input['total_stock'] = Stock::getTotalStock($data['book_id'], $data['year']);
	            $input['book_sales'] = Sale::getBookSales($data['book_id'], $data['year']);
	            $input['book'] = Book::getBook($data['book_id']);
	            $input['year'] = $data['year'];

	            $this->output = array(
	            	'total_sales' => $input['total_sales'],
	            	'total_stock' => $input['total_stock'],
	            	'in_stock' => $input['total_stock'] - $input['total_sales'],
	            	'output' => View::make('sale.ajax.sellout', $input)->render(),
	            	'year' => $data['year']
	            );

	            return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}