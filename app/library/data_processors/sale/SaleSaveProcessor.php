<?php
	
	class SaleSaveProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$sale = new Sale();
				$sale->book_id = $data['book_id'];
				$sale->sale_date = $data['sale_date'];
				$sale->sale_amount = $data['sale_amount'];
				$sale->save();

				$data['book'] = Book::find($data['book_id']);
				$data['sale'] = $sale;
				$this->output = View::make('sale.ajax.output', $data)->render();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}