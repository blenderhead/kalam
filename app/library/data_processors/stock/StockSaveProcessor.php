<?php
	
	use Carbon\Carbon;

	class StockSaveProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$stock = Stock::where('book_id','=',$data['book_id'])->where('created_at', '>=', Carbon::now()->subYear())->first();

				if(!$stock)
				{
					$stock = new Stock();
				}

				$stock->book_id = $data['book_id'];
				$stock->total_stock = $data['stock'];
				$stock->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}