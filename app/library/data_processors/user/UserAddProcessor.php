<?php
	
	class UserAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$user = \Sentry::getUserProvider()->create([
					'first_name' => $data['first_name'],
					'last_name' => $data['last_name'],
	                'email' => $data['email'],
	                'password' => $data['password'],
	                'activated' => TRUE,
	                'role' => $data['role'],
	                'image' => Helper::processImage($data['image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.user_upload_path'))
	            ]);
	    
	            $user_entity = User::where('id','=',$user->id)->first();
	            $user_entity->roles()->attach($data['role']);

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}