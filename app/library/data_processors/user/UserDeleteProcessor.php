<?php
	
	class UserDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$user = User::find($data['id']);

				User::where('id','=',$user->id)->delete();

				AssignedRole::where('user_id','=',$data['id'])->delete();

				Helper::deleteImage(Config::get('path.user_upload_path') . $user->image);

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}