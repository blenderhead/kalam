<?php
	
	class PermAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$permission = PermissionRole::where('permission_id',$data['permission'])->where('role_id',$data['role'])->get();

				if($permission->isEmpty())
				{
					DB::table('permission_role')->insert(array(
						'permission_id' => $data['permission'],
						'role_id' => $data['role']
					));
				}

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}