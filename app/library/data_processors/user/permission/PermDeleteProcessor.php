<?php
	
	class PermDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				PermissionRole::where('permission_id',$data['permission'])->where('role_id',$data['role'])->delete();
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}