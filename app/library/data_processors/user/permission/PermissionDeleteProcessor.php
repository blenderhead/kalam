<?php
	
	class PermissionDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				PermissionRole::where('permission_id','=',$data['id'])->delete();
				Permission::where('id','=',$data['id'])->delete();
								
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}