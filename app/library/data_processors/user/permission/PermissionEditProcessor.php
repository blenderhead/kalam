<?php
	
	class PermissionEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$permission = Permission::find($data['perm_id']);
				$permission->name = $data['perm_name'];
				$permission->display_name = $data['perm_display_name'];
				$permission->save();
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}