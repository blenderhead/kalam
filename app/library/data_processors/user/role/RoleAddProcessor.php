<?php
	
	class RoleAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$role = new Role();
				$role->name = ucfirst($data['role_name']);
				$role->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}