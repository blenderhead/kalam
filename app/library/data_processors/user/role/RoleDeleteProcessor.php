<?php
	
	class RoleDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				AssignedRole::where('role_id','=',$data['id'])->delete();
				PermissionRole::where('role_id','=',$data['id'])->delete();
				
				User::where('role','=',$data['id'])->update(array(
					'role' => 0
				));

				Role::where('id','=',$data['id'])->delete();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}