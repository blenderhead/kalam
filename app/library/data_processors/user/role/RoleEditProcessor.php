<?php
	
	class RoleEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$role = Role::find($data['role_id']);
				$role->name = ucwords($data['role_name']);
				$role->save();

				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}