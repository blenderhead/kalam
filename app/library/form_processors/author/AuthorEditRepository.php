<?php
	
	class AuthorEditRepository extends BaseRepository
	{
		private $id;
		private $name;
		private $email;
		private $gender;
		private $description;
		private $image;
		private $img_coord_x;
		private $img_coord_y;
		private $img_coord_w;
		private $img_coord_h;
		private $img_rw;

		public function getInput()
		{
			$this->id = Input::get('author_id');
			$this->name = Input::get('author_name');
			$this->email = Input::get('author_email');
			$this->gender = Input::get('author_gender');
			$this->description = Input::get('author_description');
			$this->image = Input::file('image');
			$this->img_coord_x = Input::get('x');
            $this->img_coord_y = Input::get('y');
            $this->img_w = Input::get('w');
            $this->img_h = Input::get('h');
            $this->img_rw = Input::get('rw');
		}

		public function setValidationData()
		{
			$this->data = array(
				'author_id' => $this->id,
	            'author_name' => $this->name,
	            'author_email' => $this->email,
	            'author_gender' => $this->gender,
	            'author_description' => $this->description,
	            'image' => $this->image,
	            'img_coord_x' => $this->img_coord_x,
	            'img_coord_y' => $this->img_coord_y,
	            'img_w' => $this->img_w,
	            'img_h' => $this->img_h,
	            'img_rw' => $this->img_rw,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'author_id' => 'required',
	            'author_name' => 'required',
	            'author_email' => 'email'
			);
		}
	}