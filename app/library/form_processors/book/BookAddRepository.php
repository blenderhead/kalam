<?php
	
	class BookAddRepository extends BaseRepository
	{
		private $title;
		private $original_title;
		private $author;
		private $publisher;
		private $category;
		private $code;
		//private $publish_date;
		private $contract_date;
		private $end_contract_date;
		private $contract_number;
		private $edition;
		private $isbn_number;
		private $price;
		private $first_print_date;
		private $last_print_date;
		private $description;
		private $advanced_royalty;
		
		private $image;
		private $img_coord_x;
		private $img_coord_y;
		private $img_coord_w;
		private $img_coord_h;
		private $img_rw;

		private $logo;
		private $logo_coord_x;
		private $logo_coord_y;
		private $logo_w;
		private $logo_h;
		private $logo_rw;

		private $op;

		public function getInput()
		{
			$this->title = Input::get('title');
			$this->original_title = Input::get('original_title');
			$this->author = Input::get('author');
			$this->publisher = Input::get('publisher');
			$this->category = Input::get('category');
			$this->code = Input::get('code');
			//$this->publish_date = Input::get('publish_date');
			$this->contract_date = Input::get('contract_date');
			$this->end_contract_date = Input::get('end_contract_date');
			$this->contract_number = Input::get('contract_number');
			$this->edition = Input::get('edition');
			$this->isbn_number = Input::get('isbn_number');
			$this->price = Input::get('price');
			$this->first_print_date = Input::get('first_print_date');
			$this->last_print_date = Input::get('last_print_date');
			$this->description = Input::get('description');
			$this->advanced_royalty = Input::get('advanced_royalty');

			$this->image = Input::file('book_image');
			$this->img_coord_x = Input::get('x');
            $this->img_coord_y = Input::get('y');
            $this->img_w = Input::get('w');
            $this->img_h = Input::get('h');
            $this->img_rw = Input::get('rw_avatar');

            $this->logo = Input::file('contract_image');
			$this->logo_coord_x = Input::get('x_logo');
            $this->logo_coord_y = Input::get('y_logo');
            $this->logo_w = Input::get('w_logo');
            $this->logo_h = Input::get('h_logo');
            $this->logo_rw = Input::get('rw_logo');

            $this->op = Input::get('op');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'title' => $this->title,
	            'original_title' => $this->original_title,
	            'author' => $this->author,
	            'publisher' => $this->publisher,
	            'category' => $this->category,
	            'code' => $this->code,
	            //'publish_date' => $this->publish_date,
	            'contract_date' => $this->contract_date,
	            'end_contract_date' => $this->end_contract_date,
	            'contract_number' => $this->contract_number,
	            'edition' => $this->edition,
	            'isbn_number' => $this->isbn_number,
	            'price' => $this->price,
	            'first_print_date' => $this->first_print_date,
	            'last_print_date' => $this->last_print_date,
	            'description' => $this->description,
	            'advanced_royalty' => $this->advanced_royalty,
	            'image' => $this->image,
	            'img_coord_x' => $this->img_coord_x,
	            'img_coord_y' => $this->img_coord_y,
	            'img_w' => $this->img_w,
	            'img_h' => $this->img_h,
	            'img_rw' => $this->img_rw,
	            'logo' => $this->logo,
	            'logo_coord_x' => $this->logo_coord_x,
	            'logo_coord_y' => $this->logo_coord_y,
	            'logo_w' => $this->logo_w,
	            'logo_h' => $this->logo_h,
	            'logo_rw' => $this->logo_rw,
	            'op' => $this->op,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'title' => 'required',
	            'author' => 'required',
	            'publisher' => 'required',
	            'category' => 'required',
	            'code' => 'required|unique:books,book_code',
	            'price' => 'required',
	            //'publish_date' => 'date_format:Y-m-d',
	            'contract_date' => 'date_format:Y-m-d|date_less_than:end_contract_date',
	            'end_contract_date' => 'date_format:Y-m-d|date_greater_than:contract_date',
	            'first_print_date' => "date_format:Y-m-d|check_contract_range:{$this->contract_date},{$this->end_contract_date}",
	            //'last_print_date' => 'date_format:Y-m-d'
	            'advanced_royalty' => 'numeric'
			);
		}
	}