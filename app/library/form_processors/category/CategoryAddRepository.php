<?php
	
	class CategoryAddRepository extends BaseRepository
	{
		private $name;
		private $code;

		public function getInput()
		{
			$this->name = Input::get('category_name');
			$this->code = Input::get('category_code');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'category_name' => $this->name,
	            'category_code' => $this->code
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'category_name' => 'required|unique:categories,name',
	            'category_code' => 'required|unique:categories,code',
			);
		}
	}