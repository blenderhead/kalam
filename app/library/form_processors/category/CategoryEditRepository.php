<?php
	
	class CategoryEditRepository extends BaseRepository
	{
		private $id;
		private $name;
		private $code;

		public function getInput()
		{
			$this->id = Input::get('category_id');
			$this->name = Input::get('category_name');
			$this->code = Input::get('category_code');
		}

		public function setValidationData()
		{
			$this->data = array(
				'category_id' => $this->id,
	            'category_name' => $this->name,
	            'category_code' => $this->code
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'category_id' => 'required|',
	            'category_name' => 'required|unique:categories,name,' . $this->id,
	            'category_code' => 'required|unique:categories,code,' . $this->id
			);
		}
	}