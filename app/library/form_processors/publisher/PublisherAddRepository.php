<?php
	
	class PublisherAddRepository extends BaseRepository
	{
		private $name;
		private $code;
		private $description;
		private $address;
		private $bank;
		private $account_number;
		private $image;
		private $img_coord_x;
		private $img_coord_y;
		private $img_coord_w;
		private $img_coord_h;
		private $img_rw;

		public function getInput()
		{
			$this->name = Input::get('publisher_name');
			$this->code = Input::get('publisher_code');
			$this->description = Input::get('publisher_description');
			$this->address = Input::get('publisher_address');
			$this->bank = Input::get('bank');
			$this->account_number = Input::get('publisher_account');
			$this->image = Input::file('image');
			$this->img_coord_x = Input::get('x');
            $this->img_coord_y = Input::get('y');
            $this->img_w = Input::get('w');
            $this->img_h = Input::get('h');
            $this->img_rw = Input::get('rw');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'publisher_name' => $this->name,
	            'publisher_code' => $this->code,
	            'publisher_description' => $this->description,
	            'address' => $this->address,
	            'bank' => $this->bank,
	            'account_number' => $this->account_number,
	            'image' => $this->image,
	            'img_coord_x' => $this->img_coord_x,
	            'img_coord_y' => $this->img_coord_y,
	            'img_w' => $this->img_w,
	            'img_h' => $this->img_h,
	            'img_rw' => $this->img_rw,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'publisher_name' => 'required',
	            'publisher_code' => 'required|numeric|unique:publishers,publisher_code',
	            'bank' => 'required',
	            'account_number' => 'required|numeric'
			);
		}
	}