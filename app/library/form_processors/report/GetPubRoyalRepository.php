<?php
	
	class GetPubRoyalRepository extends BaseRepository
	{
		private $year;

		public function getInput()
		{
			$this->year = Input::get('year');
		}

		public function setValidationData()
		{
			$this->data = array(
				'year' => $this->year
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'year' => 'required'
			);
		}
	}