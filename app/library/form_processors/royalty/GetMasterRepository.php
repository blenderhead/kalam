<?php
	
	class GetMasterRepository extends BaseRepository
	{
		private $year;
		private $publisher;

		public function getInput()
		{
			$this->year = Input::get('year');
			$this->publisher = Input::get('publisher');
		}

		public function setValidationData()
		{
			$this->data = array(
				'year' => $this->year,
				'publisher' => $this->publisher
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'year' => 'required'
			);
		}
	}