<?php
	
	class RoyaltyAddRepository extends BaseRepository
	{
		private $book_id;
		private $royalty;

		public function getInput()
		{
			$this->book_id = Input::get('book_id');
			$this->royalty = Input::get('royalty');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'book_id' => $this->book_id,
	            'royalty' => $this->royalty
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'book_id' => 'required',
	            'royalty' => 'required'
			);
		}
	}