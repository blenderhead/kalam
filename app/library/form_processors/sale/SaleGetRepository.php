<?php
	
	class SaleGetRepository extends BaseRepository
	{
		private $book_id;
		private $year;

		public function getInput()
		{
			$this->book_id = Input::get('book_id');
			$this->year = Input::get('year');
		}

		public function setValidationData()
		{
			$this->data = array(
				'book_id' => $this->book_id,
				'year' => $this->year
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'book_id' => 'required',
				'year' => 'required'
			);
		}
	}