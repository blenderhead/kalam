<?php
	
	class SaleSaveRepository extends BaseRepository
	{
		private $book_id;
		private $sale_date;
		private $quantity;
		
		public function getInput()
		{
			$this->book_id = Input::get('book_id');
			$this->sale_date = Input::get('sale_date');
			$this->sale_amount = Input::get('sale_amount');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'book_id' => $this->book_id,
	            'sale_date' => $this->sale_date,
	            'sale_amount' => $this->sale_amount,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'book_id' => 'required',
	            'sale_date' => 'required|dateFormat:Y-m-d',
	            'sale_amount' => "required|numeric|check_stock:{$this->sale_amount},{$this->book_id}",
			);
		}
	}