<?php
	
	class StockSaveRepository extends BaseRepository
	{
		private $book_id;
		private $stock;

		public function getInput()
		{
			$this->book_id = Input::get('book_id');
			$this->stock = Input::get('stock');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'book_id' => $this->book_id,
	            'stock' => $this->stock
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'book_id' => 'required',
	            'stock' => 'required|numeric'
			);
		}
	}