<?php
	
	class PermissionEditRepository extends BaseRepository
	{
		private $perm_id;
		private $perm_name;
		private $perm_display_name;

		public function getInput()
		{
			$this->perm_id = Input::get('perm_id');
			$this->perm_name = Input::get('perm_name');
			$this->perm_display_name = Input::get('perm_display_name');
		}

		public function setValidationData()
		{
			$this->data = array(
				'perm_id' => $this->perm_id,
	            'perm_name' => $this->perm_name,
	            'perm_display_name' => $this->perm_display_name
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'perm_id' => 'required',
	            'perm_name' => 'required|unique:permissions,name,' . $this->perm_id,
	            'perm_display_name' => 'required|unique:permissions,display_name,' . $this->perm_id,
			);
		}
	}