<?php
	
	class PermissionRepository extends BaseRepository
	{
		private $permission;
		private $role;

		public function getInput()
		{
			$this->permission = Input::get('permission');
			$this->role = Input::get('role');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'permission' => $this->permission,
	            'role' => $this->role
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'permission' => 'required',
	            'role' => 'required',
			);
		}
	}