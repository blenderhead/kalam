<?php
	
	class RoleAddRepository extends BaseRepository
	{
		private $role_name;

		public function getInput()
		{
			$this->role_name = Input::get('role_name');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'role_name' => $this->role_name
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'role_name' => 'required|unique:roles,name'
			);
		}
	}