<?php
	
	class RoleEditRepository extends BaseRepository
	{	
		private $role_id;
		private $role_name;

		public function getInput()
		{
			$this->role_id = Input::get('role_id');
			$this->role_name = Input::get('role_name');
		}

		public function setValidationData()
		{
			$this->data = array(
				'role_id' => $this->role_id,
	            'role_name' => $this->role_name
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'role_id' => 'required',
	            'role_name' => 'required|unique:roles,name,' . $this->role_id
			);
		}
	}