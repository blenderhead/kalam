<?php

	class CurrencyConverter
	{
        public $rate;

		public function setRate($from, $to)
        {
            $convert_from = $from;
            $convert_to = $to;

            $this->rate = Helper::swapCurrency("$convert_from/$convert_to");
        }

        public function convert($amount)
        {
            return round($amount / $this->rate, 2);
        }
	}