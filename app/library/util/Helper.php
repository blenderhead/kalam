<?php

	class Helper
	{
		public static function processImage($image, $x_coord, $y_coord, $width, $height, $rw, $path)
        {
            if($image) 
            {
                $image_processor = new ImageProcessor($image, $path);
                $image_processor->upload();
                $image_processor->crop($x_coord, $y_coord, $width, $height, $rw);
                $filename = $image_processor->getFilename();           
                return $filename;
            }
            else
            {
                return '';
            }
        }

        public static function updateImage($image, $x_coord, $y_coord, $width, $height, $rw, $path, $old_image)
        {   
            if($image) 
            {
                $destination = $path;

                $image_processor = new ImageProcessor($image, $destination);
                $image_processor->upload();
  
                $image_processor->crop($x_coord, $y_coord, $width, $height, $rw);
                $filename = $image_processor->getFilename();

                File::delete($path . $old_image);

                return $filename;
            }
            else
            {
                return $old_image;
            }
        }

        public static function deleteImage($filename)
        {
            File::delete($filename);
        }

        public static function processFlag($flag)
        {
            switch($flag)
            {
                case '0':
                    return 'Active';
                    break;

                case '1':
                    return 'Inactive';
                    break;
            }
        }

/*        public static function validasiContract($contract_date, $end_contract_date)
        {
            if($end_contract_date <= $contract_date)
            {
                return;
            }
        }*/

        public static function processCode($code)
        {
            if($code <= 9)
            {
                return '00' . $code;
            }
            else if($code >= 10 && $code <= 99)
            {
                return '0' . $code;
            }
            else
            {
                return $code;
            }
        }

        public static function moneyFormat($amount, $symbol = 'IDR')
        {
            return $symbol . ' ' . number_format($amount,2,'.','.');
        }

        public static function swapCurrency($currency)
        {
            
         /*   $httpAdapter = new \Ivory\HttpAdapter\FileGetContentsHttpAdapter();
            $yahooProvider = new \Swap\Provider\YahooFinanceProvider($httpAdapter);
            $swap = new \Swap\Swap($yahooProvider);
            $rate = $swap->quote($currency);
            return $rate->getValue();
         */   

            switch($currency)
            {
                case 'USD/IDR':
                    return 13295.0000;
                    break;

                case 'IDR/USD':
                    return 0.0001;
                    break;
            }
        }

        public static function convertCurrency($amount)
        {
            $converter = new CurrencyConverter();

            if(Session::get('convert_from') == 'USD')
            {
                $converter->setRate(Session::get('convert_from'), Session::get('convert_to'));
                $amount = $converter->convert($amount);
            }

            return $amount;
        }

        public static function calculateRoyalty($book_id, $year, $royalty_price, $advanced_royalty)
        {
            //$paid = (Sale::getTotalYearSales($book_id, $year) * $royalty_price) - $advanced_royalty;
            $paid = (Sale::getTotalYearSales($book_id, $year) * $royalty_price);

            if($paid < 0)
            {
                $paid = 0;
            }

            return $paid;
        }

        public static function calculatePaidRoyalty($total_royalty, $advanced_royalty, $book)
        { 
            /*  
            jika total royalti < dari adv. royalti 
            rumusnya jadi : adv. royalti - total royalti.
            paid royaltinya = 0. 
            adv. royaltinya yang berkurang
            
            jika total royalti > dari adv. royalti 
            rumusnya: total royalti - adv. royalti
            paid royaltinya = sisa dari perhitungan diatas
            */

            $paid_royalty = 0;

            if($book->edition == '1')
            {
                if($total_royalty < $advanced_royalty)
                {
                    $adv_royalty_balance = $advanced_royalty - $total_royalty;
                    $paid_royalty = 0;
                }
                else if($total_royalty > $advanced_royalty)
                {
                    $paid_royalty = $total_royalty - $advanced_royalty;
                    $adv_royalty_balance = 0;
                }
            }
            else
            {
                $first_edition = Book::where('book_code',$book->book_code)->where('edition','1')->first();

                if($total_royalty < $first_edition->adv_royalty_balance)
                {
                    $adv_royalty_balance = $first_edition->adv_royalty_balance - $total_royalty;
                    $paid_royalty = 0;
                }
                else if($total_royalty > $first_edition->adv_royalty_balance)
                {
                    $paid_royalty = $total_royalty - $first_edition->adv_royalty_balance;
                    $adv_royalty_balance = 0;
                }
            }

            $book = Book::find($book->id);
            $book->adv_royalty_balance = $adv_royalty_balance;
            $book->save();

            return $paid_royalty;
        }

        public static function calculateAdvancedRoyalty($book)
        {
            $adv_royalty = 0;

            if($book->edition == '1')
            {
                $book = Book::find($book->id);
                $adv_royalty = $book->adv_royalty;
            }
            else
            {
                $book = Book::where('book_code',$book->book_code)->where('edition','1')->first();

                $adv_royalty = $book->adv_royalty_balance;
            }

            return $adv_royalty;
        }

        public static function processPrintYear($book, $print_type)
        {
            if($print_type == 'first_print')
            {
                if($book->edition == '1')
                {
                    return $book->first_print_date;
                }
                else
                {
                    $first_edition = Book::where('book_code',$book->book_code)->where('edition','1')->first();

                    return $first_edition->first_print_date;
                }
            }
            else if($print_type == 'last_print')
            {
                if($book->edition == '1')
                {
                    return $book->first_print_date;
                }
                else
                {
                    $edition = Book::find($book->id);

                    return $edition->last_print_date;
                }
            }
        }

        public static function getSalesNominal($book_id, $year, $book_price)
        {
            return Sale::getTotalYearSales($book_id, $year) * $book_price;
        }
	}