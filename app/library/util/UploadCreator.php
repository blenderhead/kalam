<?php

	class UploadCreator
	{
		public function createDirScturct()
        {
            $image_upload_root = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;

            $user_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR;

            $book_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'book' . DIRECTORY_SEPARATOR;

            $publisher_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'publisher' . DIRECTORY_SEPARATOR;

            $author_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'author' . DIRECTORY_SEPARATOR;

            if (!File::exists($image_upload_root))
            {
                File::makeDirectory($image_upload_root, $mode = 0777, true, true);
            }

            if (!File::exists($user_upload_path))
            {
                File::makeDirectory($user_upload_path, $mode = 0777, true, true);
            }

            if (!File::exists($book_upload_path))
            {
                File::makeDirectory($book_upload_path, $mode = 0777, true, true);
            }

            if (!File::exists($publisher_upload_path))
            {
                File::makeDirectory($publisher_upload_path, $mode = 0777, true, true);
            }

            if (!File::exists($author_upload_path))
            {
                File::makeDirectory($author_upload_path, $mode = 0777, true, true);
            }
        }
	}