<?php

	class AssignedRole extends Eloquent
	{
		protected $table = 'assigned_roles';

		public function role()
		{
			return $this->belongsTo('Role');
		}
	}
