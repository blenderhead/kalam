<?php

	class Book extends Eloquent
	{
		protected $table = 'books';

		public function royalty()
		{
			return $this->hasOne('Royalty');
		}

		public function author()
		{
			return $this->belongsTo('Author');
		}

		public function publisher()
		{
			return $this->belongsTo('Publisher');
		}

		public function sales()
		{
			return $this->hasMany('Sale');
		}

		public function category()
		{
			return $this->belongsTo('Category');
		}

		public function stock()
		{
			return $this->hasMany('Stock');
		}

		public static function getBooks()
		{
			$result = array();

			$books = Book::select('books.id', 'books.title', 'books.edition', 'publishers.name as publisher_name', 'authors.name as author', 'categories.name as category', 'royalties.amount as royalty', 'stocks.total_stock as stock')
						   ->join('publishers','books.publisher_id', '=', 'publishers.id')
						   ->join('authors','books.author_id', '=', 'authors.id')
						   ->join('categories','books.category_id', '=', 'categories.id')
						   ->join('royalties','books.id', '=', 'royalties.book_id')
						   ->join('stocks','books.id', '=', 'stocks.book_id')
						   ->orderBy('books.title','ASC')
						   ->get();

			if(!$books->isEmpty())
			{
				$result = $books;
			}

			return $result;
		}

		public static function checkEdition($title,$author,$publisher,$edition,$op_type,$book_id = NULL)
		{
			switch($op_type)
			{
				case 'add':
					$book = Book::where('title','=',$title)
								  ->where('author_id','=',$author)
								  ->where('publisher_id','=',$publisher)
								  ->where('edition','=',$edition)
								  ->get();
					break;

				case 'edit':
					$book = Book::where('title','=',$title)
								  ->where('author_id','=',$author)
								  ->where('publisher_id','=',$publisher)
								  ->where('edition','=',$edition)
								  ->where('id','!=',$book_id)
								  ->get();
					break;
			}
			

			if(!$book->isEmpty())
			{
				return FALSE;
			}

			return TRUE;
		}

		public static function getBooksRoyalty()
		{
			$result = array();

			$books = Book::select('books.id', 'books.title', 'books.edition', 'publishers.name as publisher_name', 'authors.name as author', 'categories.name as category', 'royalties.amount as royalty')
						   ->join('publishers','books.publisher_id', '=', 'publishers.id')
						   ->join('authors','books.author_id', '=', 'authors.id')
						   ->orderBy('books.title','ASC')
						   ->get();

			if(!$books->isEmpty())
			{
				$result = $books;
			}

			return $result;
		}

		public static function getLastEdition($book_code)
		{
			$books = Book::select('edition')->where('book_code','=',$book_code)->orderBy('created_at', 'desc')->first();
			return $books->edition;
		}

		public static function getBook($book_id)
		{
			$book =  Book::find($book_id);

			if($book)
			{
				$book->price = Helper::convertCurrency($book->price);
			
			}

			return $book;
		}

		public static function getEditionsData($book_code)
		{
			$last_print_date = NULL;
			$edition = NULL;

			$book = Book::where('book_code',$book_code)->orderBy('created_at','DESC')->first();

			if($book)
			{
				if($book->last_print_date == '0000-00-00')
				{
					$book->last_print_date = $book->first_print_date;
				}

				$last_print_date = $book->last_print_date;
				$edition = $book->edition;
			}

			return compact('last_print_date', 'edition');
		}

		public static function getBookEditions($book_code)
		{
			$book_ids = array();

			$books = Book::where('book_code',$book_code)->get();

			$books->each(function($book) use (&$book_ids) {
				array_push($book_ids, $book->id);
			});

			return $book_ids;
		}
	}
