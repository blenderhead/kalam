<?php

	class Category extends Eloquent
	{
		protected $table = 'categories';

		public function book()
		{
			return $this->hasOne('Book');
		}
	}
