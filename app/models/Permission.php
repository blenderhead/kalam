<?php

	use Zizaco\Entrust\EntrustPermission;

	class Permission extends EntrustPermission
	{
		public function role()
		{
			return $this->belongsTo('Role');
		}
	}