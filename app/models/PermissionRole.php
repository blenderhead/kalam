<?php

	class PermissionRole extends Eloquent
	{
		protected $table = 'permission_role';

		protected $fillable = array('permission_id, role_id');

		public function role()
		{
			return $this->hasMany('Role');
		}
	}
