<?php

	class Publisher extends Eloquent
	{
		protected $table = 'publishers';

		public function books()
		{
			return $this->hasMany('Book');
		}
		
		public static function getPublisherCodes()
		{
			$result = array();

			$codes = Publisher::select('publisher_code')->get();

			if(!$codes->isEmpty())
			{
				$codes->each(function($code) use (&$result) {
					array_push($result, $code->publisher_code);
				});
			}

			return $result;
		}

		public static function getPublisherRoyalties($publisher_id, $year)
		{
			$royalties = array();

			$books = Book::where('publisher_id',$publisher_id)->get();

			$books->each(function($book) use (&$royalties, $year) {

				//$royalty_paid = Sale::getTotalYearSales($book->id, $year) * $book->royalty->royalty_from_price;

				$total_royalty = Helper::calculateRoyalty($book->id, $year, $book->royalty->royalty_from_price, $book->adv_royalty);
				$royalty_paid = Helper::calculatePaidRoyalty($total_royalty, $book->adv_royalty, $book);

				array_push($royalties, $royalty_paid);

			});

			$pub_royalty = array_sum($royalties);

			return Helper::convertCurrency($pub_royalty);
		}

		public static function getAllPublisherRoyalties($year)
		{
			$royalties = array();

			$books = Book::all();

			$books->each(function($book) use (&$royalties, $year) {

				//$royalty_paid = Sale::getTotalYearSales($book->id, $year) * $book->royalty->royalty_from_price;

				$total_royalty = Helper::calculateRoyalty($book->id, $year, $book->royalty->royalty_from_price, $book->adv_royalty);
				$royalty_paid = Helper::calculatePaidRoyalty($total_royalty, $book->adv_royalty, $book);

				array_push($royalties, $royalty_paid);

			});

			$pub_royalty = array_sum($royalties);

			return Helper::convertCurrency($pub_royalty);
		}
	}
