<?php
	
	use Zizaco\Entrust\EntrustRole;
	
	class Role extends EntrustRole
	{
		protected $table = 'roles';

		public function permissions()
		{
			return $this->belongs_to('PermissionRole');
		}

		public static function getRoleName($role_id)
		{
			$role = Role::where('id','=',$role_id)->get();

			foreach($role as $data)
			{
				$role_name = $data->name;
			}

			return $role_name;
		}

		public static function getRoleId($role_name)
		{
			$role = Role::where('name','=',$role_name)->get();

			foreach($role as $data)
			{
				$role_id = $data->id;
			}

			return $role_id;
		}

		public static function getRoles()
		{
			$roles = Role::where('name','!=','Admin')->get();
			return $roles;
		}

		public static function getPermissions($role_id)
		{
			$result = array();

			$permissions = PermissionRole::where('role_id',$role_id)->get();

			if(!$permissions->isEmpty())
			{
				$permissions->each(function($permission) use (&$result) {
					array_push($result, $permission->permission_id);
				});
			}

			return $result;
		}
	}