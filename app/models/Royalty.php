<?php

	class Royalty extends Eloquent
	{
		protected $table = 'royalties';

		public function book()
        {
            return $this->belongsTo('Book');
        }

        public static function getRoyalty($book_id)
        {
        	$royalty = NULL;

        	$data = Royalty::where('book_id','=',$book_id)->first();

        	if($data)
        	{
        		$royalty = $data->amount;
        	}

        	return $royalty;
        }
	}
