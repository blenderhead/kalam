<?php

	class Sale extends Eloquent
	{
		protected $table = 'sales';

		public function book()
		{
			return $this->belongsTo('Book');
		}

		public static function getTotalSales($book_id, $year)
		{
			$total = 0;

			$datas = Sale::where('book_id','=',$book_id)->where( DB::raw('YEAR(sale_date)'), '=', $year)->get();

			if(!$datas->isEmpty())
			{
				$datas->each(function($data) use(&$total, &$sale_amount) {
					$sale_amount[] = $data->sale_amount;
				});

				$total = array_sum($sale_amount);
			}

			return $total;
		}

		public static function getBookSales($book_id, $year)
        {
			$datas = Sale::where('book_id','=',$book_id)->where( DB::raw('YEAR(sale_date)'), '=', $year)->get();
			return $datas;
        }

        public static function getTotalYearSales($book_id, $year)
		{
			$the_sales = array();
			$total_sales = 0;

			//$book_ids = Book::getBookEditions($book_code);
			
			//$sales = Sale::whereIn('book_id',$book_ids)->where(DB::raw('YEAR(created_at)'), '=', $year)->get();

			$sales = Sale::where('book_id',$book_id)->where(DB::raw('YEAR(created_at)'), '=', $year)->get();

			if(!$sales->isEmpty())
			{
				$sales->each(function($sale) use (&$the_sales) {
					array_push($the_sales, $sale->sale_amount);
				});	

				$total_sales = array_sum($the_sales);
			}

			return $total_sales;
		}
	}
