<?php

	class Stock extends Eloquent
	{
		protected $table = 'stocks';

        public function book()
        {
            return $this->belongsTo('Book');
        }
        
		public static function getStock($book_id)
        {
        	$stock = NULL;

        	$data = Stock::where('book_id','=',$book_id)->first();

        	if($data)
        	{
        		$stock = $data->total_stock;
        	}

        	return $stock;
        }

        public static function getTotalStock($book_id, $year)
        {
            $total = 0;
            $stock = array();

            $datas = Stock::select('total_stock')->where('book_id','=',$book_id)->where(DB::raw('YEAR(created_at)'), '=', $year)->get();

            if(!$datas->isEmpty())
            {
                $datas->each(function($data) use(&$total, &$stock, $year) {
                    array_push($stock, $data->total_stock);
                });

                $total = array_sum($stock);
            }

            return $total;
        }

        public static function getTotalPrints($book_id, $year)
        {
            $the_stocks = array();
            $total_stocks = 0;

            //$book_ids = Book::getBookEditions($book_code);

            //$stocks = Stock::whereIn('book_id',$book_ids)->where(DB::raw('YEAR(created_at)'), '=', $year)->get();

            $stocks = Stock::where('book_id',$book_id)->where(DB::raw('YEAR(created_at)'), '=', $year)->get();

            if(!$stocks->isEmpty())
            {
                 $stocks->each(function($stock) use (&$the_stocks) {
                    array_push($the_stocks, $stock->total_stock);
                }); 

                $total_stocks = array_sum($the_stocks);
            }

            return $total_stocks;
        }
	}
