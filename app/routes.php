<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
Route::get('/', function()
{
	return View::make('hello');
});
*/

Route::get('/test', array(
    'uses' => 'TestController@getIndex'
));

Route::get('/', array(
    'uses' => 'AuthController@getLogin'
));

Route::get('/login', array(
    'as' => 'auth.login',
    'uses' => 'AuthController@getLogin'
));

Route::post('/login', array(
    'as' => 'auth.do.login',
    'uses' => 'AuthController@postLogin'
));

Route::get('logout', array(
    'as' => 'auth.logout',
    'uses' => 'AuthController@getLogout'
));

Route::group(array('prefix' => 'backend', 'before' => 'authorized'), function() {

    Route::group(array('prefix' => 'dashboard'), function() {

        Route::get('/',array(
            'as' => 'dashboard.index',
            'uses' => 'DashboardController@getIndex'
        ));

    });

	Route::group(array('prefix' => 'publisher', 'before' => 'input_data'), function() {

		Route::get('/',array(
            'as' => 'publisher.index',
            'uses' => 'PublisherController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'publisher.add',
            'uses' => 'PublisherController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'publisher.create',
            'uses' => 'PublisherController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'publisher.edit',
            'uses' => 'PublisherController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'publisher.edit',
            'uses' => 'PublisherController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'publisher.delete',
            'uses' => 'PublisherController@postDelete'
        ));

	});

	Route::group(array('prefix' => 'author', 'before' => 'input_data'), function() {
		
		Route::get('/',array(
            'as' => 'author.index',
            'uses' => 'AuthorController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'author.add',
            'uses' => 'AuthorController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'author.create',
            'uses' => 'AuthorController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'author.edit',
            'uses' => 'AuthorController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'author.edit',
            'uses' => 'AuthorController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'author.delete',
            'uses' => 'AuthorController@postDelete'
        ));

	});

	Route::group(array('prefix' => 'book', 'before' => 'input_data'), function() {
		
		Route::get('/',array(
            'as' => 'book.index',
            'uses' => 'BookController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'book.add',
            'uses' => 'BookController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'book.create',
            'uses' => 'BookController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'book.edit',
            'uses' => 'BookController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'book.edit',
            'uses' => 'BookController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'book.delete',
            'uses' => 'BookController@postDelete'
        ));

        Route::get('/add-edition',array(
            'as' => 'book.add-edition',
            'uses' => 'BookController@getAddEdition'
        ));

        Route::post('/add-edition',array(
            'as' => 'book.add-edition.save',
            'uses' => 'BookController@postAddEdition'
        ));

        Route::get('/edit-edition',array(
            'as' => 'book.edit-edition',
            'uses' => 'BookController@getEditEdition'
        ));

        Route::post('/edit-edition',array(
            'as' => 'book.edit-edition.save',
            'uses' => 'BookController@postEditEdition'
        ));
	});

	Route::group(array('prefix' => 'category', 'before' => 'input_data'), function() {
		
		Route::get('/',array(
            'as' => 'category.index',
            'uses' => 'CategoryController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'category.add',
            'uses' => 'CategoryController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'category.create',
            'uses' => 'CategoryController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'category.edit',
            'uses' => 'CategoryController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'category.edit',
            'uses' => 'CategoryController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'category.delete',
            'uses' => 'CategoryController@postDelete'
        ));

	});

	Route::group(array('prefix' => 'royalty', 'before' => 'input_data'), function() {
		
        Route::get('/',array(
            'as' => 'royalty.index',
            'uses' => 'RoyaltyController@getIndex'
        ));

        Route::post('/save',array(
            'as' => 'royalty.save',
            'uses' => 'RoyaltyController@postSave'
        ));

	});

	Route::group(array('prefix' => 'report', 'before' => 'view_report'), function() {
		
        Route::get('/master',array(
            'as' => 'report.master',
            'uses' => 'ReportController@getMaster'
        ));

        Route::post('/master',array(
            'as' => 'report.master.get.data',
            'uses' => 'ReportController@postMaster'
        ));

        Route::get('/per_publisher',array(
            'as' => 'report.per_publisher',
            'uses' => 'ReportController@getPerPublisher'
        ));

        Route::post('/per_publisher',array(
            'as' => 'report.per_publisher.get_data',
            'uses' => 'ReportController@postPerPublisher'
        ));

        Route::get('/generate-master-print',array(
            'as' => 'report.master.generate-print',
            'uses' => 'ReportController@generateMasterPrint'
        ));

        Route::get('/generate-master-pdf',array(
            'as' => 'report.master.generate-pdf',
            'uses' => 'ReportController@generateMasterPdf'
        ));

        Route::get('/generate-pub-print',array(
            'as' => 'report.publisher.generate-print',
            'uses' => 'ReportController@generatePublisherPrint'
        ));

        Route::get('/generate-pub-pdf',array(
            'as' => 'report.publisher.generate-pdf',
            'uses' => 'ReportController@generatePublisherPdf'
        ));

	});

	Route::group(array('prefix' => 'user', 'before' => 'admin'), function() {

		Route::get('/',array(
            'as' => 'user.index',
            'uses' => 'UserController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'user.add',
            'uses' => 'UserController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'user.create',
            'uses' => 'UserController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'user.edit',
            'uses' => 'UserController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'user.edit',
            'uses' => 'UserController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'user.delete',
            'uses' => 'UserController@postDelete'
        ));

        Route::group(array('prefix' => 'role'), function() {

            Route::get('/',array(
                'as' => 'user.role.index',
                'uses' => 'RoleController@getIndex'
            ));

            Route::get('/add',array(
                'as' => 'user.role.add',
                'uses' => 'RoleController@getCreate'
            ));

            Route::post('/add',array(
                'as' => 'user.role.add.save',
                'uses' => 'RoleController@postCreate'
            ));
            
            Route::get('/edit',array(
                'as' => 'user.role.edit',
                'uses' => 'RoleController@getEdit'
            ));

            Route::post('/edit',array(
                'as' => 'user.role.edit.save',
                'uses' => 'RoleController@postEdit'
            ));

            Route::post('/delete',array(
                'as' => 'user.role.delete',
                'uses' => 'RoleController@postDelete'
            ));

        });

        Route::group(array('prefix' => 'permission'), function() {

            Route::get('/',array(
                'as' => 'user.permission.index',
                'uses' => 'PermissionController@getIndex'
            ));

            Route::get('/add',array(
                'as' => 'user.permission.add',
                'uses' => 'PermissionController@getCreate'
            ));

            Route::post('/add',array(
                'as' => 'user.permission.add.save',
                'uses' => 'PermissionController@postCreate'
            ));

            Route::get('/edit',array(
                'as' => 'user.permission.edit',
                'uses' => 'PermissionController@getEdit'
            ));

            Route::post('/edit',array(
                'as' => 'user.permission.edit.save',
                'uses' => 'PermissionController@postEdit'
            ));

            Route::post('/delete',array(
                'as' => 'user.permission.delete',
                'uses' => 'PermissionController@postDelete'
            ));

        });

        Route::group(array('prefix' => 'role-permission'), function() {

            Route::get('/',array(
                'as' => 'user.role-permission.index',
                'uses' => 'RolePermissionController@getIndex'
            ));

            Route::post('/add',array(
                'as' => 'user.permission.add',
                'uses' => 'RolePermissionController@postSetPermission'
            ));

            Route::post('/delete',array(
                'as' => 'user.permission.delete',
                'uses' => 'RolePermissionController@postDeletePermission'
            ));

        });

    });

    Route::group(array('prefix' => 'stock', 'before' => 'input_stock'), function() {
        
        Route::get('/',array(
            'as' => 'stock.index',
            'uses' => 'StockController@getIndex'
        ));

        Route::post('/save',array(
            'as' => 'stock.save',
            'uses' => 'StockController@postSave'
        ));

    });

    Route::group(array('prefix' => 'sale', 'before' => 'input_sale'), function() {
        
        Route::get('/',array(
            'as' => 'sale.index',
            'uses' => 'SaleController@getIndex'
        ));

        Route::get('/edit',array(
            'as' => 'sale.edit',
            'uses' => 'SaleController@getEdit'
        ));

        Route::post('/save',array(
            'as' => 'sale.save',
            'uses' => 'SaleController@postSave'
        ));

        Route::post('/delete',array(
            'as' => 'sale.delete',
            'uses' => 'SaleController@postDelete'
        ));

        Route::post('/get',array(
            'as' => 'sale.get',
            'uses' => 'SaleController@postGetSale'
        ));

        Route::get('/generate-print',array(
            'as' => 'sale.generate-print',
            'uses' => 'SaleController@generatePrint'
        ));

        Route::get('/generate-pdf',array(
            'as' => 'sale.generate-pdf',
            'uses' => 'SaleController@generatePdf'
        ));

    });

    Route::group(array('prefix' => 'currency'), function() {
        
        Route::get('/set',array(
            'as' => 'currency.set',
            'uses' => 'CurrencyController@setCurrency'
        ));

    });

});
