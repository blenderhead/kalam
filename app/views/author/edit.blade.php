@extends('layouts.backend')

@section('title')
	Author - Edit
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Edit Author
@stop

@section('page_description')
	edit author form
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="edit-author" method="post" enctype="multipart/form-data" data-op="edit">		

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="author_name" class="col-lg-2 control-label">Author Name</label>
							<div class="col-lg-8">
								<input type="text" class="form-control author_name" id="author_name" placeholder="author name" name="author_name" value="{{ $author->name }}">
							</div>
						</div>
							
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="author_email" class="col-lg-2 control-label">Author Email</label>
							<div class="col-lg-8">
								<input type="text" class="form-control author_email" id="author_email" placeholder="author email" name="author_email" value="{{ $author->email }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="author_gender" class="col-lg-2 control-label">Author Gender</label>
							<div class="col-lg-8">
								<select class="author_gender" name="author_gender">
									<option value="male" @if($author->gender == 'male'){{'selected'}}@endif>Male</option>
									<option value="female" @if($author->gender == 'female'){{'selected'}}@endif>Female</option>
								</select>
							</div>
						</div>
							
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="author_description" class="col-lg-2 control-label">Description</label>
							<div class="col-lg-8">
								<textarea class="form-control bs-texteditor author_description" rows="7" name="author_description"></textarea>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="author_image" class="col-lg-2 control-label">Author Image</label>
							<div class="col-lg-8">
								<input type="file" class="author_image" id="author_image" name="image" data-op="edit">

								<div class="logo-container">
		                            <img id="preview-normal" @if($author->image){{'src="' . URL::to('/') . '/uploads/author/' . $author->image . '"'}}@endif />
		                            <input type="hidden" id="x" name="x" />
		                            <input type="hidden" id="y" name="y" />
		                            <input type="hidden" id="w" name="w" />
		                            <input type="hidden" id="h" name="h" />
		                        </div>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" name="author_id" value="{{ $author->id }}" class="author_id" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/author' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/classes/author/Author.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/AuthorManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {
			resetFormElement($("#author_image"));
			
			$(".author_description").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});

			$(".author_gender").selectpicker();

			$('.author_description').val("{{ $author->description }}");

		});
	</script>
@stop