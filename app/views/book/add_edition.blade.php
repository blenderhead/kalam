@extends('layouts.backend')

@section('title')
	Book - Edit
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Edit Book
@stop

@section('page_description')
	edit book form
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="add-edition" method="post" enctype="multipart/form-data" data-op="add-edition">

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_title" class="col-lg-2 control-label">Title</label>
							<div class="col-lg-8">
								<input disabled type="text" class="form-control book_title" id="book_title" placeholder="book title" name="title" value="{{ $book->title }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="original_title" class="col-lg-2 control-label">Original Title</label>
							<div class="col-lg-8">
								<input disabled type="text" class="form-control original_title" id="original_title" placeholder="original book title" name="original_title" value="{{ $book->original_title }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_author" class="col-lg-2 control-label">Author</label>
							<div class="col-lg-8">
								<select class="book_author" name="author" disabled>
									@foreach($authors as $author)
										<option value="{{ $author->id }}" @if($book->author_id == $author->id){{'selected'}}@endif>{{ $author->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_publisher" class="col-lg-2 control-label">Publisher</label>
							<div class="col-lg-8">
								<select class="book_publisher" name="publisher" disabled>
									@foreach($publishers as $publisher)
										<option value="{{ $publisher->id }}" @if($book->publisher_id == $publisher->id){{'selected'}}@endif>{{ $publisher->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
							
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_category" class="col-lg-2 control-label">Category</label>
							<div class="col-lg-8">
								<select class="book_category" name="category" disabled>
									@foreach($categories as $category)
										<option value="{{ $category->id }}" @if($book->category_id == $category->id){{'selected'}}@endif>{{ $category->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_publish" class="col-lg-2 control-label">Publish Date</label>
							<div class="col-lg-8">
								<input type="text" class="form-control datepicker book_publish" id="book_publish" name="publish_date" value="{{ $book->published_date }}">
							</div>
						</div>
						-->

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_contract" class="col-lg-2 control-label">Contract Date</label>
							<div class="col-lg-8">
								<input type="text" class="form-control datepicker book_contract" id="book_contract" name="contract_date" value="{{ $book->contact_date }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="end_contract_date" class="col-lg-2 control-label">End Contract Date</label>
							<div class="col-lg-8">
								<input type="text" class="form-control datepicker end_contract_date" id="end_contract_date" name="end_contract_date" placeholder="end contract date" value="{{ date('Y-m-d', strtotime($book->end_contract_date)) }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_contract_number" class="col-lg-2 control-label">Contract Number</label>
							<div class="col-lg-8">
								<input type="text" class="form-control book_contract_number" id="book_contract_number" placeholder="contract number" name="contract_number" value="{{ $book->contact_number }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="advanced_royalty" class="col-lg-2 control-label">Advanced Royalty</label>
							<div class="col-lg-8">
								<input type="text" class="form-control advanced_royalty" id="advanced_royalty" placeholder="advanced royalty" name="advanced_royalty" value="{{ $book->adv_royalty }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_edition" class="col-lg-2 control-label">Edition</label>
							<div class="col-lg-8">
								<select class="book_edition" name="edition">
									<option value={{$last_edition + 1}}>{{$last_edition + 1}}</option>
								</select>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_isbn" class="col-lg-2 control-label">ISBN Number</label>
							<div class="col-lg-8">
								<input type="text" class="form-control book_isbn" id="book_isbn" placeholder="isbn number" name="isbn_number" value="{{ $book->isbn }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_price" class="col-lg-2 control-label">Price</label>
							<div class="col-lg-8">
								<input type="text" class="form-control book_price" id="book_price" placeholder="price" name="price" value="{{ $book->price }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="book_last_print" class="col-lg-2 control-label">Last Print Date</label>
							<div class="col-lg-8">
								<input type="text" class="form-control datepicker book_last_print" id="book_last_print" name="last_print_date">
							</div>
						</div>
							
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="author_description" class="col-lg-2 control-label">Description</label>
							<div class="col-lg-8">
								<textarea class="form-control bs-texteditor author_description" rows="7" name="description"></textarea>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="author_image" class="col-lg-2 control-label">Book Cover</label>
							<div class="col-lg-8">
								<input type="file" class="book_image" id="book_image" name="book_image" data-op="add">

								<div class="logo-container">
		                            <img id="image-preview" />
		                            <input type="hidden" id="x" name="x" />
		                            <input type="hidden" id="y" name="y" />
		                            <input type="hidden" id="w" name="w" />
		                            <input type="hidden" id="h" name="h" />
		                        </div>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="contract_image" class="col-lg-2 control-label">Contract Image</label>
							<div class="col-lg-8">
								<input type="file" class="contract_image" id="contract_image" name="contract_image" data-op="add">

								<div class="logo-container">
		                            <img id="logo-preview" />
		                            <input type="hidden" id="x_logo" name="x_logo" />
		                            <input type="hidden" id="y_logo" name="y_logo" />
		                            <input type="hidden" id="w_logo" name="w_logo" />
		                            <input type="hidden" id="h_logo" name="h_logo" />
		                        </div>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" id="op" name="op" value="add-edition" />
							<input type="hidden" name="book_id" value="{{ $book->id }}" class="book_id" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/book' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<!-- datepicker -->
    <script src="{{ asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ asset('assets/js/classes/book/Book.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/BookManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			resetFormElement($("#book_image"));
			resetFormElement($("#contract_image"));

			$(".author_description").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});

			$(".datepicker").datepicker({
				'format': 'yyyy-mm-dd'
			});

			$(".book_category, .book_edition, .book_author, .book_publisher").selectpicker();

			$('.author_description').val("{{ $book->description }}");

		});
	</script>
@stop