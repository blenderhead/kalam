@extends('layouts.backend')

@section('title')
	Book - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Book
@stop

@section('page_description')
	books list
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title bg-white no-border">
                	<div class="panel-tools">
						<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<!-- <th>ID</th> -->
								<th>Code</th>
								<th>Title</th>
								<th>Original Title</th>
								<th>Author</th>
								<th>Publisher</th>
								<th>Category</th>
								<th>Edition</th>
								<th>End Contract</th>
								<th>Status</th>
								<th>Operations</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($books as $book)
								<tr>
									<!-- <td>{{ $book->id }}</td> -->
									<td>{{ $book->publisher->publisher_code . $book->category->code . $book->book_code }}</td>
									<td>{{ $book->title }}</td>
									<td>{{ $book->original_title}}</td>
									<td>{{ $book->author->name }}</td>
									<td>{{ $book->publisher->name }}</td>
									<td>{{ $book->category->name }}</td>
									<td>{{ $book->edition }}</td>
									<td>{{ $book->end_contract_date }}</td>
									<td>
										@if( (date('Y-m-d', strtotime('+60 days'))) >= $book->end_contract_date)
											RENEW CONTRACT
										@else
											CLEAR
										@endif
									</td>
									<td>
										@if($book->edition == '1')
											<a href="{{ URL::to('/') . '/backend/book/edit?id=' . $book->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
											@if($book->end_contract_date > date('Y-m-d'))
												<a href="{{ URL::to('/') . '/backend/book/add-edition?id=' . $book->id }}" type="button" class="btn btn-success btn-circle add-edition" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-plus"></i></a>
											@else
												<a href="#" type="button" class="btn btn-success btn-circle add-edition" data-id="{{ $book->id }}" Title="CONTRACT EXPIRED"><i class="glyphicon glyphicon-plus"></i></a>
											@endif
										@endif



										<!-- <button type="button" class="btn btn-warning btn-circle delete" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-remove"></i></button> -->
				
										@if($book->edition > '1')
											<a href="{{ URL::to('/') . '/backend/book/edit-edition?id=' . $book->id }}" type="button" class="btn btn-success btn-circle edit-edition" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										@endif
										<!-- @if($book->edition == '1' and $book->end_contract_date > date('Y-m-d H:i:s'))
											<a href="{{ URL::to('/') . '/backend/book/edit?id=' . $book->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										@else
											<a href="{{ URL::to('/') . '/backend/book/edit-edition?id=' . $book->id }}" type="button" class="btn btn-success btn-circle edit-edition" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										@endif
 -->
<!-- 										<button type="button" class="btn btn-warning btn-circle delete" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-remove"></i></button> -->
																					
<!-- 										@if($book->edition == '1' and $book->end_contract_date > date('Y-m-d H:i:s'))
											<a href="{{ URL::to('/') . '/backend/book/add-edition?id=' . $book->id }}" type="button" class="btn btn-success btn-circle add-edition" data-id="{{ $book->id }}" title="Expired"><i class="glyphicon glyphicon-plus"></i></a>
										@else -->
											<!-- <a href="#" data-toggle="tooltip" title="Expired"><i class="ion-gear-a"></i></a> -->
											<!-- CONTRACT EXPIRED -->
											<!-- <a href="#" type="button" class="btn btn-success btn-circle add-edition" data-id="{{ $book->id }}" title="Expired"><i class="glyphicon glyphicon-plus"></i></a>											
										@endif -->
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<!-- <th>ID</th> -->
								<th>Code</th>
								<th>Title</th>
								<th>Original Title</th>
								<th>Author</th>
								<th>Publisher</th>
								<th>Category</th>
								<th>Edition</th>
								<th>End Contract</th>
								<th>Status</th>
								<th>Operations</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/book/Book.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/BookManager.js') }}" type="text/javascript"></script>
@stop