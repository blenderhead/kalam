@extends('layouts.backend')

@section('title')
	Category - Edit
@stop

@section('styles')
@stop

@section('page_title')
	Edit Category
@stop

@section('page_description')
	edit category form
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="edit-category" method="post" enctype="multipart/form-data" data-op="edit">

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="category_name" class="col-lg-2 control-label">Category Name</label>
							<div class="col-lg-8">
								<input type="text" class="form-control category_name" id="category_name" placeholder="category name" name="category_name" value="{{ $category->name }}">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="category_name" class="col-lg-2 control-label">Category Code</label>
							<div class="col-lg-8">
								<input type="text" class="form-control category_code" id="category_code" placeholder="category code" name="category_code" value="{{ $category->code }}">
							</div>
						</div>
							
						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" name="category_id" value="{{ $category->id }}" class="category_id" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/category' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ asset('assets/js/classes/category/Category.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/CategoryManager.js') }}" type="text/javascript"></script>

@stop