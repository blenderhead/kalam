@extends('layouts.backend')

@section('title')
	Category - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Category
@stop

@section('page_description')
	categories list
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title bg-white no-border">
                	<div class="panel-tools">
						<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Code</th>
								<th>Status</th>
								<th>Operations</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($categories as $category)
								<tr>
									<td>{{ $category->id }}</td>
									<td>{{ $category->name }}</td>
									<td>{{ $category->code }}</td>
									<td>{{ Helper::processFlag($category->is_deleted) }}</td>
									<td>
										<a href="{{ URL::to('/') . '/backend/category/edit?id=' . $category->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $category->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										<button type="button" class="btn btn-warning btn-circle delete" data-id="{{ $category->id }}"><i class="glyphicon glyphicon-remove"></i></button>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Code</th>
								<th>Status</th>
								<th>Operations</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/category/Category.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/CategoryManager.js') }}" type="text/javascript"></script>
@stop