@extends('layouts.backend')

@section('title')
	Publisher - Add
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Add Publisher
@stop

@section('page_description')
	add publisher form
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="add-publisher" method="post" enctype="multipart/form-data" data-op="add">	

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="publisher_name" class="col-lg-2 control-label">Publisher Name</label>
							<div class="col-lg-8">
								<input type="text" class="form-control publisher_name" id="publisher_name" placeholder="publisher name" name="publisher_name">
							</div>
						</div>
							
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="publisher_code" class="col-lg-2 control-label">Publisher Code</label>
							<div class="col-lg-8">
								<input type="text" class="form-control publisher_code" id="publisher_code" name="publisher_code" placeholder="publisher code">
							</div>
						</div>
							
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="publisher_description" class="col-lg-2 control-label">Description</label>
							<div class="col-lg-8">
								<textarea class="form-control bs-texteditor publisher_description" rows="7" name="publisher_description"></textarea>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="publisher_address" class="col-lg-2 control-label">Address</label>
							<div class="col-lg-8">
								<textarea class="form-control bs-texteditor publisher_address" rows="7" name="publisher_address"></textarea>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="bank" class="col-lg-2 control-label">Bank</label>
							<div class="col-lg-8">
								<input type="text" class="form-control bank" id="bank" placeholder="bank name" name="bank">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="publisher_account" class="col-lg-2 control-label">Account Number</label>
							<div class="col-lg-8">
								<input type="text" class="form-control publisher_account" id="publisher_account" placeholder="bank account number" name="publisher_account">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="publisher_logo" class="col-lg-2 control-label">Logo</label>
							<div class="col-lg-8">
								<input type="file" class="publisher_logo" id="publisher_logo" name="image" data-op="add">

								<div class="logo-container">
		                            <img id="logo-preview" />
		                            <input type="hidden" id="x" name="x" />
		                            <input type="hidden" id="y" name="y" />
		                            <input type="hidden" id="w" name="w" />
		                            <input type="hidden" id="h" name="h" />
		                        </div>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/publisher' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script type="text/javascript">
		var publisher_codes = {{ $publisher_codes }}
	</script>
	
	<script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/classes/publisher/Publisher.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/PublisherManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {
			
			resetFormElement($("#publisher_logo"));

			$(".publisher_description").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			})
		});
	</script>
@stop