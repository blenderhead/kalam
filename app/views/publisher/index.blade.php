@extends('layouts.backend')

@section('title')
	Publisher - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Publisher
@stop

@section('page_description')
	publishers list
@stop

@section('content')
	<div class="row">
        <div class="col-lg-12">
			<div class="panel no-border ">
                <div class="panel-title bg-white no-border">
					<div class="panel-tools">
						<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Publisher Name</th>
								<th>Publisher Code</th>
								<th>Address</th>
								<th>Status</th>
								<th>Operation</th>
							</tr>
						</thead>
						<tbody>
							@foreach($publishers as $publisher)
								<tr>
									<td>{{ $publisher->name }}</td>
									<td>{{ $publisher->publisher_code }}</td>
									<td>{{ $publisher->address }}</td>
									<td>{{ Helper::processFlag($publisher->is_deleted) }}</td>
									<td>
										<a href="{{ URL::to('/') . '/backend/publisher/edit?id=' . $publisher->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $publisher->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										<!-- <a type="button" class="btn btn-warning btn-circle delete" data-id="{{ $publisher->id }}"><i class="glyphicon glyphicon-remove"></i></a> -->
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Publisher Name</th>
								<th>Publisher Code</th>
								<th>Address</th>
								<th>Status</th>
								<th>Operation</th>
							</tr>
						</tfoot>
					</table>
            	</div>
        	</div><!-- /.col -->
    </div><!-- /. row -->
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/publisher/Publisher.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/PublisherManager.js') }}" type="text/javascript"></script>
@stop