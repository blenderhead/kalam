<script type="text/javascript">
    

    $(document).ready(function() {
      $('.table').DataTable();
    });
</script> 

<div class="panel-footer bg-white padding-top-20" style="border: none !important;">
  <a href="{{ URL::to('/') . '/backend/report/generate-master-print?year=' . $year . '&publisher=' . $publisher_filter }}" target="_blank" class="btn btn-default"><i class="ion-printer margin-right-5"></i> Print</button>
  <a href="{{ URL::to('/') . '/backend/report/generate-master-pdf?year=' . $year . '&publisher=' . $publisher_filter }}" target="_blank" class="btn btn-primary pull-right margin-right-10"><i class="ion-document margin-right-5"></i> Generate PDF</a>
</div>

<div class="padding-md margin-bottom-20" style="background: #FFF;">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Code</th>
        <th>Title</th>
        <th>Original Title</th>
        <th>Author</th>
        <th>Pub</th>
        <th>First Print</th>
        <th>Last Print</th>
        <th>Edition</th>
        <th>Printed</th>
        <th>Royalty</th>
        <th>Stock</th>
        <th>Sale</th>
        <th>Sale Nominal</th>
        <th>Adv. Royalty</th>
        <th>Total Royalty</th>
        <th>Royalty Paid</th>
        <th>Adv. Royalty Balance</th>
      </tr>
    </thead>
    <tbody>
    	@foreach($books as $book)
        <tr>
          <th scope="row">{{ $book->publisher->publisher_code . $book->category->code . $book->book_code }}</th>
          <td>{{ $book->title }}</td>
          <td>{{ $book->original_title }}</td>
          <td>{{ $book->author->name }}</td>
          <td>{{ $book->publisher->name }}</td>
          <td>{{ $book->first_print_date }}</td>
          <td>{{ $book->last_print_date }}</td>
          <td>{{ $book->edition }}</td>
          <td>{{ $book->printed }}</td>
          <td>{{ $book->royalty->amount }}%</td>
          <td>{{ $book->stock }}</td>
          <td>{{ $book->sales }}</td>
          <td>{{ Helper::moneyFormat($book->sales_nominal, $default_currency) }}</td>
          <td>{{ Helper::moneyFormat($book->adv_royalty, $default_currency) }}</td>
          <td>{{ Helper::moneyFormat($book->total_royalty, $default_currency) }}</td>
          <td>{{ Helper::moneyFormat($book->royalty_paid, $default_currency) }}</td>
          <td>{{ Helper::moneyFormat($book->adv_royalty_balance, $default_currency) }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>