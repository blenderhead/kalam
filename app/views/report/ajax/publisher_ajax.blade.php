<div class="bg-amber-50 padding-md margin-bottom-20 margin-top-20">
	<h4 class="margin-bottom-30 color-grey-700">Publishers Royalties summary</h4>
	<div class="table-responsive">
		<table class="table sale-summary">
			<thead>
				<tr>
					<td class="hidden-xs"><strong>Publisher</strong></td>
					<td class="hidden-xs"><strong>Year</strong></td>
					<td class="hidden-xs"><strong>Royalties</strong></td>
				</tr>
			</thead>
			<tbody>
				@foreach($publishers as $publisher)
					<tr>
						<td class="hidden-xs">{{ $publisher->name }}</td>
						<td>{{ $year }}</td>
						<td>{{ Helper::moneyFormat(Publisher::getPublisherRoyalties($publisher->id,$year), $default_currency) }}</td>
					</tr>
				@endforeach
				<tr>
					<td class="no-border"></td>
					<td class="no-border"><strong>Total</strong></td>
					<td class="no-border"><strong>{{ Helper::moneyFormat($total_royalties,$default_currency) }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="panel-footer bg-white padding-top-20" style="border: none !important;">
	<a href="{{ URL::to('/') . '/backend/report/generate-pub-print?year=' . $year }}" target="_blank" class="btn btn-default"><i class="ion-printer margin-right-5"></i> Print</button>
	<a href="{{ URL::to('/') . '/backend/report/generate-pub-pdf?year=' . $year }}" target="_blank" class="btn btn-primary pull-right margin-right-10"><i class="ion-document margin-right-5"></i> Generate PDF</a>
</div>