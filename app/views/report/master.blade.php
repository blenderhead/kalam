
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title>Master Report</title>
	
	<!-- BEGIN CORE FRAMEWORK -->
	<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<!-- END CORE FRAMEWORK -->
	
	<!-- BEGIN PLUGIN STYLES -->
	<link href="{{ asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN THEME STYLES -->
	<link href="{{ asset('assets/css/material.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/plugins.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/helpers.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" />
	<!-- END THEME STYLES -->
</head>

<body class="bg-grey-100">

    <div class="wrapper">
		
    	<div class="row">

    		<div class="col-lg-12">

    			<div class="sale-summary-wrapper">

    				<div class="padding-md margin-bottom-20 margin-top-20">

    					<a href="{{ URL::to('/') }}" type="button" class="btn btn-success btn-circle add-edition" data-id=""><i class="glyphicon glyphicon-home"></i></a>

    					<h2 class="margin-bottom-30 color-grey-700">Report summary</h4>

    					<p>
	    					Report Year:
	    					<select class="year">
								@for($i=2000;$i<=date('Y', time());$i++)
									<option value="{{ $i }}" @if(date('Y', time() == $i)){{'selected'}}@endif>{{ $i }}</option>
								@endfor
							</select>

						</p>

						<p>
	    					Filter by Publisher:
	    					<select class="publisher">
	    						<option value="0">All publisher</option>
								@foreach($publishers as $publisher)
									<option value="{{ $publisher->id }}">{{ $publisher->name }}</option>
								@endforeach
							</select>

						</p>

						<p>

							Currency:
							<!--
							<a type="button" href="#" class="set-currency btn btn-circle" data-from="IDR" data-to="USD"><i class="glyphicon glyphicon-floppy-disk"></i></a>
							<a type="button" href="#" class="set-currency btn btn-circle" data-from="USD" data-to="IDR"><i class="glyphicon glyphicon-floppy-disk"></i></a>
							-->
							<select class="set-currency">
								<option value="IDR" @if(Session::get('convert_from') == 'IDR'){{'selected'}}@endif>IDR</option>
								<option value="USD" @if(Session::get('convert_from') == 'USD'){{'selected'}}@endif>USD</option>
							</select>
						
							<a>
								@if(Session::get('convert_from') == 'USD')
									1 USD = {{ Helper::moneyFormat(Helper::swapCurrency('USD/IDR'), 'IDR') }}
								@elseif(Session::get('convert_from') == 'IDR')
									<!-- 1 IDR = {{ Helper::moneyFormat(Helper::swapCurrency('IDR/USD'), 'USD') }} -->
								@endif
							</a>	
						</p>

						
						<br />
						<br />

    					<div class="table-responsive report-summary-wrapper">

			    			

						</div>

					</div>
				
				</div>

    		</div>

    	</div>

	</div>

	
	<!-- BEGIN CORE PLUGINS -->
	<script src="{{ asset('assets/plugins/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/bootstrap/js/holder.js') }}"></script>
	<script src="{{ asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/core.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/functions.js') }}" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	
	<!-- counter -->
	<script src="{{ asset('assets/plugins/jquery-countTo/jquery.countTo.js') }}" type="text/javascript"></script>

	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>
	
	<!-- maniac -->
	<script src="{{ asset('assets/js/maniac.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var baseUrl = "{{ URL::to('/') }}";
		maniac.loadcounter();
		$(".year, .set-currency, .publisher").selectpicker();
	</script> 
	
	<script src="{{ asset('assets/js/classes/report/Report.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/ReportManager.js') }}" type="text/javascript"></script>

	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>