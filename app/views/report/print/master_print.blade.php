
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title>Master Report</title>
	
	<!-- BEGIN CORE FRAMEWORK -->
	<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<!-- END CORE FRAMEWORK -->
	
	<!-- BEGIN PLUGIN STYLES -->
	<link href="{{ asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN THEME STYLES -->
	<link href="{{ asset('assets/css/material.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/plugins.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/helpers.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" />
	<!-- END THEME STYLES -->
</head>

<body class="bg-grey-100">

    <div class="wrapper">
		
    	<div class="row">

    		<div class="col-lg-12">

    			<div class="sale-summary-wrapper">

    				<div class="padding-md margin-bottom-20 margin-top-20">

    					<h2 class="margin-bottom-30 color-grey-700">Report summary</h4>

    					<p>
	    					Report Year: {{ $year }}
						</p>

						@if($publisher)
							<p>
								<address>
									<strong>Publisher:</strong> {{ $publisher->name }}<br>
									<strong>Code:</strong> {{ $publisher->publisher_code }}<br>
									<strong>Bank Account:</strong> {{ ucwords($publisher->bank) }}<br>
									<strong>Account Number:</strong> {{ ucwords($publisher->account_number) }}<br>
									<strong>Address:</strong> {{ $publisher->address }}<br>
								</address>
							</p>
						@endif

						<p>
							@if(Session::get('convert_from') == 'USD')
								1 USD = {{ Helper::moneyFormat(Helper::swapCurrency('USD/IDR'), 'IDR') }}
							@elseif(Session::get('convert_from') == 'IDR')
								1 IDR = {{ Helper::moneyFormat(Helper::swapCurrency('IDR/USD'), 'USD') }}
							@endif

						</p>

						<br />
						<br />


						<div class="panel-footer bg-white padding-top-20 padding-bottom-20" style="border: none !important;">
							<button class="btn btn-default" onclick="window.print();"><i class="ion-printer margin-right-5"></i> Print</button>
						</div>

    					<div class="table-responsive report-summary-wrapper">

			    			<div class="padding-md margin-bottom-20" style="background: #FFF;">
							  <table class="table table-striped">
							    <thead>
							      <tr>
							        <th>Code</th>
							        <th>Title</th>
							        <th>Original Title</th>
							        <th>Author</th>
							        <th>Pub</th>
							        <th>First Print</th>
							        <th>Last Print</th>
							        <th>Edition</th>
							        <th>Printed</th>
							        <th>Royalty</th>
							        <th>Stock</th>
							        <th>Sale</th>
							        <th>Sale Nominal</th>
							        <th>Adv Royalty</th>
							        <th>Total Royalty</th>
							        <th>Royalty Paid</th>
							        <th>Adv. Royalty Balance</th>
							      </tr>
							    </thead>
							    <tbody>
							    	@foreach($books as $book)
							        <tr>
							          <th scope="row">{{ $book->publisher->publisher_code . $book->category->code . $book->book_code }}</th>
							          <td>{{ $book->title }}</td>
							          <td>{{ $book->original_title }}</td>
							          <td>{{ $book->author->name }}</td>
							          <td>{{ $book->publisher->name }}</td>
							          <td>{{ $book->first_print_date }}</td>
							          <td>{{ $book->last_print_date }}</td>
							          <td>{{ $book->edition }}</td>
							          <td>{{ $book->printed }}</td>
							          <td>{{ $book->royalty->amount }}%</td>
							          <td>{{ $book->stock }}</td>
							          <td>{{ $book->sales }}</td>
							          <td>{{ Helper::moneyFormat($book->sales_nominal, $default_currency) }}</td>
							          <td>{{ Helper::moneyFormat($book->adv_royalty, $default_currency) }}</td>
							          <td>{{ Helper::moneyFormat($book->total_royalty, $default_currency) }}</td>
							          <td>{{ Helper::moneyFormat($book->royalty_paid, $default_currency) }}</td>
							          <td>{{ Helper::moneyFormat($book->adv_royalty_balance, $default_currency) }}</td>
							        </tr>
							      @endforeach
							    </tbody>

							  </table>

							</div>

						</div>

						<p>
							Date: {{ date('Y-m-d H:i:s', time()) }}
						</p>
						<p>
							Generated by: {{ $credential->first_name . ' ' . $credential->last_name }}
						</p>

					</div>
				
				</div>

    		</div>

    	</div>

	</div>

	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>