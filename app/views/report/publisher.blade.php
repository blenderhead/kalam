@extends('layouts.backend')

@section('title')
	Publisher Royalties - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Publisher Royalties
@stop

@section('page_description')
	publishers roylties summary report
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

    		<div class="panel no-shadow">

				<div class="panel-body no-padding">
					<div class="border-bottom-1 border-grey-100 padding-bottom-20 margin-bottom-20 clearfix">
					<!--<h4 class="pull-right no-margin">Order # 12345</h4>-->
				</div>

				<div class="row border-bottom-1 border-grey-100 margin-bottom-20">
					<div class="col-xs-6">
						<address>
							<strong>Sale Year:</strong>
								<select class="year">
									@for($i=2000;$i<=date('Y', time());$i++)
										<option value="{{ $i }}" @if(date('Y', time() == $i)){{'selected'}}@endif>{{ $i }}</option>
									@endfor
								</select><br>
						</address>
					</div>
				</div>

				<div class="publisher-summary-wrapper">
					<div class="bg-amber-50 padding-md margin-bottom-20 margin-top-20">
						<h4 class="margin-bottom-30 color-grey-700">Publishers Royalties summary</h4>
						<div class="table-responsive">
							<table class="table sale-summary">
								<thead>
									<tr>
										<td class="hidden-xs"><strong>Publisher</strong></td>
										<td class="hidden-xs"><strong>Year</strong></td>
										<td class="hidden-xs"><strong>Royalties</strong></td>
									</tr>
								</thead>
								<tbody>
									@foreach($publishers as $publisher)
										<tr>
											<td class="hidden-xs">{{ $publisher->name }}</td>
											<td>{{ $year }}</td>
											<td>{{ Helper::moneyFormat(Publisher::getPublisherRoyalties($publisher->id,$year), $default_currency) }}</td>
										</tr>
									@endforeach
									<tr>
										<td class="no-border"></td>
										<td class="no-border"><strong>Total</strong></td>
										<td class="no-border"><strong>{{ Helper::moneyFormat($total_royalties,$default_currency) }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="panel-footer bg-white padding-top-20" style="border: none !important;">
						<a href="{{ URL::to('/') . '/backend/report/generate-pub-print?year=' . $year }}" target="_blank" class="btn btn-default"><i class="ion-printer margin-right-5"></i> Print</button>
						<a href="{{ URL::to('/') . '/backend/report/generate-pub-pdf?year=' . $year }}" target="_blank" class="btn btn-primary pull-right margin-right-10"><i class="ion-document margin-right-5"></i> Generate PDF</a>
					</div>

				</div>
			</div>

			
		</div>
		</div>
    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
		$(".year").selectpicker();
	</script>

	<script src="{{ asset('assets/js/classes/report/Report.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/ReportManager.js') }}" type="text/javascript"></script>
@stop