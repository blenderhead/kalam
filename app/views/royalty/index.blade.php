@extends('layouts.backend')

@section('title')
	Book - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Book
@stop

@section('page_description')
	books list
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title bg-white no-border">
                	<div class="panel-tools">
						<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>ID</th>
								<th>Code</th>
								<th>Title</th>
								<th>Author</th>
								<th>Publisher</th>
								<th>Edition</th>
								<th>Set Royalty</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($books as $book)
								<tr>
									<td>{{ $book->id }}</td>
									<td>{{ $book->publisher->publisher_code . $book->category->code . $book->book_code }}</td>
									<td>{{ $book->title }}</td>
									<td>{{ $book->author->name }}</td>
									<td>{{ $book->publisher->name }}</td>
									<td>{{ $book->edition }}</td>
									<td>
										<input type="text" class="form-control book-royalty-{{$book->id}}" id="book_title" placeholder="enter royalty amount" name="title" value="{{ $book->royalty['amount'] }}">
										<button type="button" class="btn btn-success btn-circle save" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-floppy-disk"></i></button>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Code</th>
								<th>Title</th>
								<th>Author</th>
								<th>Publisher</th>
								<th>Edition</th>
								<th>Set Royalty</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/royalty/Royalty.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/RoyaltyManager.js') }}" type="text/javascript"></script>
@stop