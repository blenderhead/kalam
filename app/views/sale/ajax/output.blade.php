<tr>
	<td class="hidden-xs">{{ $sale->sale_date }}</td>
	<td class="text-center">{{ $sale->sale_amount }}</td>
	<td class="text-center">{{ $book->price }}</td>
	<td class="text-right">{{ $book->price * $sale->sale_amount}}</td>
</tr>