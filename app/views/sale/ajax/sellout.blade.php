<div class="bg-amber-50 padding-md margin-bottom-20 margin-top-20">
	<h4 class="margin-bottom-30 color-grey-700">Sales summary</h4>
	<div class="table-responsive">
		<table class="table sale-summary">
			<thead>
				<tr>
					<td class="hidden-xs"><strong>Sale Date</strong></td>
					<td class="text-center"><strong>Quantity</strong></td>
					<td class="text-center"><strong>Price</strong></td>
					<td class="text-right"><strong>Totals</strong></td>
					<td class="text-right"><strong>Operation</strong></td>
				</tr>
			</thead>
			<tbody>
				@foreach($book_sales as $sales)
					<tr>
						<td class="hidden-xs">{{ $sales->sale_date }}</td>
						<td class="text-center">{{ $sales->sale_amount }}</td>
						<td class="text-center"><strong>{{ Helper::moneyFormat($book->price, $default_currency) }}</td>
						<td class="text-right"><strong>{{  Helper::moneyFormat($book->price * $sales->sale_amount, $default_currency) }}</td>
						@if($year == date('Y', time()))
							<td class="text-right"><button type="button" class="btn btn-warning btn-circle delete" data-id="{{ $sales->id }}"><i class="glyphicon glyphicon-trash"></i></button></td>
						@endif
					</tr>
				@endforeach
				<tr>
					<td class="no-border hidden-xs"></td>
					<td class="no-border"></td>
					<td class="no-border text-center"><strong>Total</strong></td>
					<td class="no-border text-right"><strong>{{ Helper::moneyFormat($total_sales * $book->price, $default_currency) }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="panel-footer bg-white padding-top-20 padding-bottom-20">
	<a href="{{ URL::to('/') . '/backend/sale/generate-print?book_id=' . $book->id . '&year=' . $year }}" target="_blank" class="btn btn-default"><i class="ion-printer margin-right-5"></i> Print</button>
	<a href="{{ URL::to('/') . '/backend/sale/generate-pdf?book_id=' . $book->id . '&year=' . $year }}" target="_blank" class="btn btn-primary pull-right margin-right-10"><i class="ion-document margin-right-5"></i> Generate PDF</a>
</div>