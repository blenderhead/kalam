@extends('layouts.backend')

@section('title')
	Sale - Edit
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
@stop
@stop

@section('page_title')
	
@stop

@section('page_description')
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

    		<div class="panel no-shadow">

				<div class="panel-body no-padding">
					<div class="border-bottom-1 border-grey-100 padding-bottom-20 margin-bottom-20 clearfix">
					<h3 class="no-margin display-inline-block pull-left"><i class="ion-ios-bolt margin-right-5"></i> {{ $book->title }}</h3>
					<!--<h4 class="pull-right no-margin">Order # 12345</h4>-->
				</div>

				<div class="row border-bottom-1 border-grey-100 margin-bottom-20">
					<div class="col-xs-6">
						<address>
							<strong>Code:</strong> {{ $book->publisher->publisher_code. $book->book_code . $book->category->code }}<br>
							<strong>Author:</strong> {{ $book->author->name }}<br>
							<strong>Publisher:</strong> {{ $book->publisher->name }}<br>
							<strong>Edition:</strong> {{ $book->edition }}<br>
							<strong>Sale Year:</strong>
								<select class="sale_year" data-book-id="{{ $book->id }}">
									@for($i=2000;$i<=date('Y', time());$i++)
										<option value="{{ $i }}" @if(date('Y', time() == $i)){{'selected'}}@endif>{{ $i }}</option>
									@endfor
								</select><br>
							<strong>Total Stock:</strong> <span class="total_stock">{{ $total_stock }}</span><br>
							<strong>In Stock:</strong> <span class="total_instock">{{ $total_stock - $total_sales }}</span>
						</address>
					</div>
				</div>

				<div class="row border-bottom-1 border-grey-100 margin-bottom-20 padding-bottom-20 clearfix sale-input-wrapper">
					<div class="col-lg-3">
						<input type="text" class="form-control datepicker sale_date" id="sale_date" placeholder="enter sale date">
					</div>	

					<div class="col-lg-3">
						<input type="text" class="form-control sale_amount" id="sale_amount" placeholder="enter sale quantity">
					</div>

					<div class="col-lg-3">
						<input type="hidden" name="book_id" value="{{ $book->id }}" class="book_id" />
						<button type="button" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Save</button>
					</div>

				</div>

				<div class="sale-summary-wrapper">
					<div class="bg-amber-50 padding-md margin-bottom-20 margin-top-20">
						<h4 class="margin-bottom-30 color-grey-700">Sales summary</h4>
						<div class="table-responsive">
							<table class="table sale-summary">
								<thead>
									<tr>
										<td class="hidden-xs"><strong>Sale Date</strong></td>
										<td class="text-center"><strong>Quantity</strong></td>
										<td class="text-center"><strong>Price</strong></td>
										<td class="text-right"><strong>Totals</strong></td>
										<td class="text-right"><strong>Operation</strong></td>
									</tr>
								</thead>
								<tbody>
									@foreach($book_sales as $sales)
										<tr>
											<td class="hidden-xs">{{ $sales->sale_date }}</td>
											<td class="text-center">{{ $sales->sale_amount }}</td>
											<td class="text-center"><strong>{{ Helper::moneyFormat($book->price, $default_currency) }}</td>
											<td class="text-right"><strong>{{  Helper::moneyFormat($book->price * $sales->sale_amount, $default_currency) }}</td>
											<td class="text-right"><button type="button" class="btn btn-warning btn-circle delete" data-id="{{ $sales->id }}"><i class="glyphicon glyphicon-trash"></i></button></td>
										</tr>
									@endforeach
									<tr>
										<td class="no-border hidden-xs"></td>
										<td class="no-border"></td>
										<td class="no-border text-center"><strong>Total</strong></td>
										<td class="no-border text-right"><strong>{{ Helper::moneyFormat($total_sales * $book->price, $default_currency) }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
							
					<div class="panel-footer bg-white padding-top-20 padding-bottom-20">
						<!--<button class="btn btn-default" onclick="window.print();"><i class="ion-printer margin-right-5"></i> Print</button>-->
						<a href="{{ URL::to('/') . '/backend/sale/generate-print?book_id=' . $book->id . '&year=' . $year }}" target="_blank" class="btn btn-default"><i class="ion-printer margin-right-5"></i> Print</button>
						<a href="{{ URL::to('/') . '/backend/sale/generate-pdf?book_id=' . $book->id . '&year=' . $year }}" target="_blank" class="btn btn-primary pull-right margin-right-10"><i class="ion-document margin-right-5"></i> Generate PDF</a>
					</div>
				</div>
			</div>
		</div>
    </div>
@stop

@section('scripts')
	<script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

	<script src="{{ asset('assets/js/classes/sale/Sale.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/SaleManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			$("#sale_date").datepicker({
				'format': 'yyyy-mm-dd'
			});
		});
	</script>
@stop