@extends('layouts.backend')

@section('title')
	Sales Management - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Sales Management
@stop

@section('page_description')
	index page
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title bg-white no-border">
                	<div class="panel-tools">
						<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>ID</th>
								<th>Code</th>
								<th>Title</th>
								<th>Edition</th>
								<th>Operations</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($books as $book)
								<tr>
									<td>{{ $book->id }}</td>
									<td>{{ $book->publisher->publisher_code . $book->category->code . $book->book_code }}</td>
									<td>{{ $book->title }}</td>
									<td>{{ $book->edition }}</td>
									<td>
										<a href="{{ URL::to('/') . '/backend/sale/edit?id=' . $book->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-cog"></i></a>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Code</th>
								<th>Title</th>
								<th>Edition</th>
								<th>Operations</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/sale/Sale.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/SaleManager.js') }}" type="text/javascript"></script>
@stop