@extends('layouts.backend')

@section('title')
	Stock - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Stock
@stop

@section('page_description')
	stock list
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title bg-white no-border">
                	<div class="panel-tools">
						<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>ID</th>
								<th>Code</th>
								<th>Title</th>
								<th>Author</th>
								<th>Publisher</th>
								<th>Edition</th>
								<th>Stock Year</th>
								<th>Set Stock</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($books as $book)
								<tr>
									<td>{{ $book->id }}</td>
									<td>{{ $book->publisher->publisher_code . $book->category->code . $book->book_code }}</td>
									<td>{{ $book->title }}</td>
									<td>{{ $book->author->name }}</td>
									<td>{{ $book->publisher->name }}</td>
									<td>{{ $book->edition }}</td>
									<td>{{ date('Y', time()) }}</td>
									<td>
										<input type="text" class="form-control book-stock-{{$book->id}}" id="book_stock" placeholder="enter book stock here" name="book_stock" value="{{ Stock::getTotalStock($book->id, date('Y', time())); }}">
										<button type="button" class="btn btn-success btn-circle save" data-id="{{ $book->id }}"><i class="glyphicon glyphicon-floppy-disk"></i></button>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Code</th>
								<th>Title</th>
								<th>Author</th>
								<th>Publisher</th>
								<th>Edition</th>
								<th>Stock Year</th>
								<th>Set Stock</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/stock/Stock.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/StockManager.js') }}" type="text/javascript"></script>
@stop