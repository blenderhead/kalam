@extends('layouts.backend')

@section('title')
	User - Add
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Add User
@stop

@section('page_description')
	add user form
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="add-user" method="post" enctype="multipart/form-data" data-op="add">

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="user_first_name" class="col-lg-2 control-label">First Name</label>
							<div class="col-lg-8">
								<input type="text" class="form-control user_first_name" id="user_first_name" placeholder="first name" name="first_name">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="user_last_name" class="col-lg-2 control-label">Last Name</label>
							<div class="col-lg-8">
								<input type="text" class="form-control user_last_name" id="user_last_name" placeholder="last name" name="last_name">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="user_email" class="col-lg-2 control-label">Email</label>
							<div class="col-lg-8">
								<input type="text" class="form-control user_email" id="user_email" placeholder="email" name="email">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="user_password" class="col-lg-2 control-label">Password</label>
							<div class="col-lg-8">
								<input type="password" class="form-control user_password" id="user_password" placeholder="password" name="password">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="user_password_confirm" class="col-lg-2 control-label">Password Confirm</label>
							<div class="col-lg-8">
								<input type="password" class="form-control user_password_confirm" id="user_password_confirm" placeholder="password confirm" name="password_confirm">
							</div>
						</div>
							
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="user_role" class="col-lg-2 control-label">Role</label>
							<div class="col-lg-8">
								<select class="user_role" name="role">
									@foreach($roles as $role)
										<option value="{{ $role->id }}">{{ $role->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="user_avatar" class="col-lg-2 control-label">Avatar</label>
							<div class="col-lg-8">
								<input type="file" class="author_image" id="author_image" name="image" data-op="add">

								<div class="logo-container">
		                            <img id="logo-preview" />
		                            <input type="hidden" id="x" name="x" />
		                            <input type="hidden" id="y" name="y" />
		                            <input type="hidden" id="w" name="w" />
		                            <input type="hidden" id="h" name="h" />
		                        </div>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/user' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ asset('assets/js/classes/user/User.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/UserManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			resetFormElement($("#author_image"));
			
			$(".user_role").selectpicker()

		});
	</script>
@stop