@extends('layouts.backend')

@section('title')
	User - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	User
@stop

@section('page_description')
	users list
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title bg-white no-border">
                	<div class="panel-tools">
						<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
					</div>
				</div>

                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Role</th>
								<th>Operations</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($users as $user)
								<tr>
									<td>{{ $user->id }}</td>
									<td>{{ $user->first_name . ' ' . $user->last_name }}</td>
									<td>{{ Role::getRoleName($user->role) }}</td>
									<td>
										<a type="button" href="{{ URL::to('/') . '/backend/user/edit?id=' . $user->id }}" class="btn btn-success btn-circle edit" data-id="{{ $user->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										<a type="button" class="btn btn-primary btn-circle delete" data-id="{{ $user->id }}" data-type="user"><i class="glyphicon glyphicon-remove"></i></a>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Operations</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/user/User.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/UserManager.js') }}" type="text/javascript"></script>
@stop