@extends('layouts.backend')

@section('title')
	Permission - Add
@stop

@section('styles')
@stop

@section('page_title')
	Add Permission
@stop

@section('page_description')
	add permission form
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="add-permission" method="post" enctype="multipart/form-data" data-op="add">

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="perm_name" class="col-lg-2 control-label">Name</label>
							<div class="col-lg-8">
								<input type="text" class="form-control perm_name" id="perm_name" placeholder="permission name. e.g: add_category" name="perm_name">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="v" class="col-lg-2 control-label">Description</label>
							<div class="col-lg-8">
								<input type="text" class="form-control perm_display_name" id="perm_display_name" placeholder="permission description" name="perm_display_name">
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/user/permission' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ asset('assets/js/classes/user/User.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/UserManager.js') }}" type="text/javascript"></script>

@stop