@extends('layouts.backend')

@section('title')
	Permissions - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Permissions
@stop

@section('page_description')
	permissions list
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

				<div class="panel-title bg-white no-border">
					<a href="{{ URL::to('/') . '/backend/user/permission/add' }}" type="button" class="btn btn-success btn-icon-left margin-right-5"><i class="fa fa-check"></i> Add Permission</a>
				</div>
				
                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>ID</th>
								<th>Permission</th>
								<th>Actions</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($permissions as $permission)
								<tr>
									<td>{{ $permission->id }}</td>
									<td>{{ $permission->display_name }}</td>
									<td>
										<a href="{{ URL::to('/') . '/backend/user/permission/edit?id=' . $permission->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $permission->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										<button type="button" class="btn btn-warning btn-circle delete" data-id="{{ $permission->id }}" data-type="permission"><i class="glyphicon glyphicon-remove"></i></button>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Permission</th>
								<th>Actions</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/user/User.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/UserManager.js') }}" type="text/javascript"></script>
@stop