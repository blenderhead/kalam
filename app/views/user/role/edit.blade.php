@extends('layouts.backend')

@section('title')
	Role - Edit
@stop

@section('styles')
@stop

@section('page_title')
	Edit Role
@stop

@section('page_description')
	edit role form
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="edit-role" method="post" enctype="multipart/form-data" data-op="edit">

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="role_name" class="col-lg-2 control-label">Role Name</label>
							<div class="col-lg-8">
								<input type="text" class="form-control role_name" id="role_name" placeholder="role name. e.g: Stockist" name="role_name" value="{{ $role->name }}">
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" name="role_id" value="{{ $role->id }}" class="role_id" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/user/role' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ asset('assets/js/classes/user/User.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/UserManager.js') }}" type="text/javascript"></script>

@stop