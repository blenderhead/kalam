@extends('layouts.backend')

@section('title')
	Roles - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Roles
@stop

@section('page_description')
	roles list
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

				<div class="panel-title bg-white no-border">
					<a href="{{ URL::to('/') . '/backend/user/role/add' }}" type="button" class="btn btn-success btn-icon-left margin-right-5"><i class="fa fa-check"></i> Add Role</a>
				</div>
				
                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>ID</th>
								<th>Role</th>
								<th>Actions</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($roles as $role)
								<tr>
									<td>{{ $role->id }}</td>
									<td>{{ $role->name }}</td>
									<td>
										<a href="{{ URL::to('/') . '/backend/user/role/edit?id=' . $role->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $role->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										<button type="button" class="btn btn-warning btn-circle delete" data-id="{{ $role->id }}" data-type="role"><i class="glyphicon glyphicon-remove"></i></button>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Role</th>
								<th>Actions</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/user/User.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/UserManager.js') }}" type="text/javascript"></script>
@stop