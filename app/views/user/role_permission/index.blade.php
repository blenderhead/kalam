@extends('layouts.backend')

@section('title')
	Role Permissions - Index
@stop

@section('styles')
	<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Role Permissions
@stop

@section('page_description')
	role permissions list
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

				<div class="panel-title bg-white no-border">
                	<div class="panel-tools">
						<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
					</div>
				</div>
				
                <div class="panel-body no-padding-top bg-white">

					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th>ID</th>
								<th>Role</th>
								<th>Permissions</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($roles as $role)
								<tr>
									<td>{{ $role->id }}</td>
									<td>{{ $role->name }}</td>
									<td>
										@foreach($permissions as $permission)
											<p>
												<input type="checkbox" class="permission"

												data-role-id="{{ $role->id }}"

												@foreach(Role::getPermissions($role->id) as $assigned_permission)
													@if($assigned_permission == $permission->id)
														{{ 'checked' }}
													@endif
												@endforeach

												value="{{ $permission->id }}" /><span class="perm_name">{{ $permission->display_name }}</span>
											</p>
										@endforeach
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Role</th>
								<th>Permissions</th>
							</tr>
						</tfoot>

					</table>

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<!-- datatables -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ asset('assets/js/classes/user/User.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/classes/UserManager.js') }}" type="text/javascript"></script>
@stop