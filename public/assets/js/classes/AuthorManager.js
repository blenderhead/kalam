var author = new Author();

$(document).ready(function() {

	$('#add-author, #edit-author').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
        	case 'add':
        		var url = baseUrl + '/backend/author/add';
        		var redirect_url = url;
        		var rw = $('#logo-preview').width();
        		break;

        	case 'edit':
        		var url = baseUrl + '/backend/author/edit';
        		var redirect_url = baseUrl + '/backend/author/edit?id=' + $('.author_id').val();
        		var rw = $('#preview-normal').width();
        		break;
        }
        
        form_data.append('rw', rw);
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    showSuccess('Author is successfully saved', redirect_url);
                }

            }
        });
    });

	$("#author_image").change(function(){
        
        var op = $(this).data('op');

        switch(op)
        {
        	case 'add':
        		var logo_preview = $('#logo-preview');
        		break;

        	case 'edit':
        		var logo_preview = $('#preview-normal');
        		break;
        }

        var JcropAPI = logo_preview.data('Jcrop');

        if(JcropAPI != undefined)
        {
            JcropAPI.destroy();
        }
        
        file = readURL(this, logo_preview, 'avatar');
          
    });

    $('body').on('click', '.delete', function() {
    	var id = $(this).data('id');
    	var url = baseUrl + '/backend/author/delete';
    	var redirect_url = baseUrl + '/backend/author';
    	deleteData(id, url, redirect_url);
    });

});