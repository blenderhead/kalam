var book = new Book();

$(document).ready(function() {
 
	$('#add-book, #edit-book, #add-edition, #edit-edition').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
        	case 'add':
        		var url = baseUrl + '/backend/book/add';
        		var redirect_url = url;
        		var rw_avatar = $('#image-preview').width();
                var rw_logo = $('#logo-preview').width();
        		break;

        	case 'edit':
        		var url = baseUrl + '/backend/book/edit';
        		var redirect_url = baseUrl + '/backend/book/edit?id=' + $('.book_id').val();
        		var rw_avatar = $('#preview-normal').width();
                var rw_logo = $('#logo-normal').width();
        		break;

            case 'add-edition':
                var url = baseUrl + '/backend/book/add-edition';
                var redirect_url = baseUrl + '/backend/book';
                var rw_avatar = $('#image-preview').width();
                var rw_logo = $('#logo-preview').width();
                break;

            case 'edit-edition':
                var url = baseUrl + '/backend/book/edit-edition';
                var redirect_url = baseUrl + '/backend/book/edit-edition?id=' + $('.book_id').val();
                var rw_avatar = $('#preview-normal').width();
                var rw_logo = $('#logo-normal').width();
                break;
        }
        
        form_data.append('rw_avatar', rw_avatar);
        form_data.append('rw_logo', rw_logo);
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    showSuccess('Book is successfully saved', redirect_url);
                }

            }
        });
    });

	$("#book_image").change(function(){
        
        var op = $(this).data('op');

        switch(op)
        {
            case 'add':
                var preview = $('#image-preview');
                break;

            case 'edit':
                var preview = $('#preview-normal');
                break;
        }

        var JcropAPI = preview.data('Jcrop');

        if(JcropAPI != undefined)
        {
            JcropAPI.destroy();
        }

        file = readURL(this, preview, 'avatar');
          
    });

    $("#contract_image").change(function(){
        
        var op = $(this).data('op');

        switch(op)
        {
            case 'add':
                var preview = $('#logo-preview');
                break;

            case 'edit':
                var preview = $('#logo-normal');
                break;
        }

        var JcropAPI = preview.data('Jcrop');

        if(JcropAPI != undefined)
        {
            JcropAPI.destroy();
        }

        file = readURL(this, preview, 'logo');
          
    });

});