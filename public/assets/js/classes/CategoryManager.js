var category = new Category();

$(document).ready(function() {

	$('#add-category, #edit-category').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
        	case 'add':
        		var url = baseUrl + '/backend/category/add';
        		var redirect_url = url;
        		break;

        	case 'edit':
        		var url = baseUrl + '/backend/category/edit';
        		var redirect_url = baseUrl + '/backend/category/edit?id=' + $('.category_id').val();
        		break;
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    showSuccess('Category is successfully saved', redirect_url);
                }

            }
        });
    });

    $('body').on('click', '.delete', function() {
    	var id = $(this).data('id');
    	var url = baseUrl + '/backend/category/delete';
    	var redirect_url = baseUrl + '/backend/category';
    	deleteData(id, url, redirect_url);
    });
});