var report = new Report();

$(document).ready(function() {

    $('.publisher').selectpicker('val', 0);

    var year = $('.year').val();
    var publisher = $('.publisher').val();

    report.getData(year);
    report.getPublisherData(year);

    $('.year').change(function() {
        var year = $(this).val();
        var publisher = $('.publisher').val();
        report.getData(year, publisher);
        report.getPublisherData(year);
    });

    $('.publisher').change(function() {
        var year = $('.year').val();
        var publisher = $(this).val();
        report.getData(year, publisher);
        report.getPublisherData(year);
    });

    $('.set-currency').change(function(e) {

        e.preventDefault();

        var from = $(this).val();

        switch(from)
        {
            case 'IDR':
                var to = 'USD';
                break;

            case 'USD':
                var to = 'IDR';
                break;
        }

        $.ajax({
            type: 'GET',
            dataType: 'html',
            data: { 
                'from' : from,
                'to' : to,
            },
            url: baseUrl + '/backend/currency/set',
            success: function() {
                window.location.reload();
            },
        });

    });
	
});