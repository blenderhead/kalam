var sale = new Sale();

$(document).ready(function() {
 
	$('.save').click(function() {
        sale.save();
    });

    $('body').on('click', '.delete', function() {
        var id = $(this).data('id');
        var book_id = $('.book_id').val();
        sale.delete(id, book_id);
    });

    $('body').on('change', '.sale_year', function() {
        var year = $(this).val();
        var book_id = $(this).data('book-id');
        sale.getSale(year, book_id);
    });
});