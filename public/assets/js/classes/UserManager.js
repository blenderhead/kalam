var user = new User();

$(document).ready(function() {
 	
	$('#login-form').submit(function(e) {

        e.preventDefault();

        var form_data = new FormData(this);
        
		$.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseUrl + '/login',
            data: form_data,
            processData: false,
            contentType: false,
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    window.location.replace(baseUrl + '/backend/dashboard');
                }

            }
        });
    });

    $('#add-user, #edit-user').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = baseUrl + '/backend/user/add';
                var redirect_url = url;
                var rw = $('#logo-preview').width();
                break;

            case 'edit':
                var url = baseUrl + '/backend/user/edit';
                var redirect_url = baseUrl + '/backend/user/edit?id=' + $('.user_id').val();
                var rw = $('#preview-normal').width();
                break;
        }
        
        form_data.append('rw', rw);
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    showSuccess('User is successfully saved', redirect_url);
                }

            }
        });
    });

    $("#author_image").change(function(){
        
        var op = $(this).data('op');

        switch(op)
        {
            case 'add':
                var logo_preview = $('#logo-preview');
                break;

            case 'edit':
                var logo_preview = $('#preview-normal');
                break;
        }

        var JcropAPI = logo_preview.data('Jcrop');

        if(JcropAPI != undefined)
        {
            JcropAPI.destroy();
        }
        
        file = readURL(this, logo_preview, 'avatar');
          
    });

    $('body').on('click', '.delete', function() {
        var id = $(this).data('id');
        var type = $(this).data('type');

        switch(type)
        {
            case 'user':
                var url = baseUrl + '/backend/user/delete';
                var redirect_url = baseUrl + '/backend/user';
                break;

            case 'role':
                var url = baseUrl + '/backend/user/role/delete';
                var redirect_url = baseUrl + '/backend/user/role';
                break;

            case 'permission':
                var url = baseUrl + '/backend/user/permission/delete';
                var redirect_url = baseUrl + '/backend/user/permission';
                break;
        }
        
        deleteData(id, url, redirect_url);
    });

    $('.permission').click(function() {
        
        var permission = $(this).val();
        var role_id = $(this).data('role-id');

        if($(this).is(":checked"))
        {
            user.setPermission(permission,role_id);
        }
        else
        {
            user.deletePermission(permission,role_id);
        }
    });

    $('#add-role, #edit-role').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = baseUrl + '/backend/user/role/add';
                var redirect_url = url;
                break;

            case 'edit':
                var url = baseUrl + '/backend/user/role/edit';
                var redirect_url = baseUrl + '/backend/user/role/edit?id=' + $('.role_id').val();
                break;
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    showSuccess('Role is successfully saved', redirect_url);
                }

            }
        });
    });

    $('#add-permission, #edit-permission').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = baseUrl + '/backend/user/permission/add';
                var redirect_url = url;
                break;

            case 'edit':
                var url = baseUrl + '/backend/user/permission/edit';
                var redirect_url = baseUrl + '/backend/user/permission/edit?id=' + $('.perm_id').val();
                break;
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    showSuccess('Permission is successfully saved', redirect_url);
                }

            }
        });
    });

});