function Report()
{

}

Report.prototype =
{
	getData: function(year,publisher) {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 
                'year' : year,
                'publisher' : publisher
            },
            url: baseUrl + '/backend/report/master',
            beforeSend: function() {
                $('.bg-grey-100').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            success: function(data) {
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    $('.report-summary-wrapper').html(data.data.output);
                }
            },
            complete: function() {
                $('.bg-grey-100').waitMe('hide');
            } 
        });

	},
    getPublisherData: function(year) {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 
                'year' : year,
            },
            url: baseUrl + '/backend/report/per_publisher',
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Getting data...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            success: function(data) {
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    $('.publisher-summary-wrapper').html(data.data.output);
                }
            },
            complete: function() {
                $('.panel').waitMe('hide');
            } 
        });

    }
}