function Royalty()
{

}

Royalty.prototype =
{
	saveRoyalty: function(book_id) {

		var royalty = $('.book-royalty-' + book_id).val();

		$.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 
                'book_id' : book_id,
                'royalty' : royalty
            },
            url: baseUrl + '/backend/royalty/save',
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            success: function(data) {
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                	var redirect_url = baseUrl + '/backend/royalty';
                    showSuccess('Royalty is successfully saved', redirect_url);
                }
            },
            complete: function() {
                $('.panel').waitMe('hide');
            } 
        });
	}
}