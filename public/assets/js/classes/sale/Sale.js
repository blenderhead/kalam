function Sale()
{

}

Sale.prototype =
{
	save: function() {

		var book_id = $('.book_id').val();
        var sale_date = $('.sale_date').val();
        var sale_amount = $('.sale_amount').val();

		$.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 
                'book_id' : book_id,
                'sale_date' : sale_date,
                'sale_amount' : sale_amount
            },
            url: baseUrl + '/backend/sale/save',
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            success: function(data) {
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                	var redirect_url = baseUrl + '/backend/sale/edit?id=' + book_id;
                    showSuccess('Sale is successfully saved', redirect_url);
                }
            },
            complete: function() {
                $('.panel').waitMe('hide');
            } 
        });
	},
    delete: function(id, book_id) {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 
                'id' : id,
            },
            url: baseUrl + '/backend/sale/delete',
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            success: function(data) {
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    var redirect_url = baseUrl + '/backend/sale/edit?id=' + book_id;
                    showSuccess('Sale is successfully deleted', redirect_url);
                }
            },
            complete: function() {
                $('.panel').waitMe('hide');
            } 
        });
    },
    getSale: function(year, book_id) {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 
                'year' : year,
                'book_id' : book_id,
            },
            url: baseUrl + '/backend/sale/get',
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            success: function(data) {
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    $('.sale-summary-wrapper').html(data.data.output.output);
                    $('.total_stock').html(data.data.output.total_stock);
                    $('.total_instock').html(data.data.output.in_stock);

                    var d = new Date();
                    var year = d.getFullYear();

                    if(data.data.output.year != year)
                    {
                        $('.sale-input-wrapper').hide();
                    }
                    else
                    {
                        $('.sale-input-wrapper').show();
                    }
                }
            },
            complete: function() {
                $('.panel').waitMe('hide');
            } 
        });
    },
}