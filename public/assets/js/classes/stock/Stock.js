function Stock()
{

}

Stock.prototype =
{
	saveStock: function(book_id) {

		var stock = $('.book-stock-' + book_id).val();

		$.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 
                'book_id' : book_id,
                'stock' : stock
            },
            url: baseUrl + '/backend/stock/save',
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            success: function(data) {
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                	var redirect_url = baseUrl + '/backend/stock';
                    showSuccess('Stock is successfully saved', redirect_url);
                }
            },
            complete: function() {
                $('.panel').waitMe('hide');
            } 
        });
	}
}