function User()
{

}

User.prototype =
{
	setPermission: function(perm,role) {
		$.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 
                'permission' : perm,
                'role' : role
            },
            url: baseUrl + '/backend/user/role-permission/add',
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            success: function(data) {
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    bootbox.alert('Permission is successfully saved', function() {});
                }
            },
            complete: function() {
                $('.panel').waitMe('hide');
            } 
        });
	},
	deletePermission: function(perm,role) {
		$.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 
                'permission' : perm,
                'role' : role
            },
            url: baseUrl + '/backend/user/role-permission/delete',
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            success: function(data) {
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    bootbox.alert('Permission is successfully deleted', function() {});
                }
            },
            complete: function() {
                $('.panel').waitMe('hide');
            } 
        });
	}
}